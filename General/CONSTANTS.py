#!/usr/bin/env python
# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # 
# C O N S T A N T S #
# # # # # # # # # # # 


"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy ODE solver 

#This code contains general constants to be called by the other Python 
#modules. It also contains the wavenumber to be investigated and the
#time parameters.
import math #importing maths functions
from math import pi as pi

############################################
#Parameters/constants for the current epoch#
############################################

G = 6.707*(10.0**(-39.0))
K = (8.0*pi*G)**0.5 # (8*pi*6.707*10**(-57))**0.5 Just 8 pi G
M = 1.0/K #10.0**(-8) # 10**(-1) # 10**(-8)? Mass of fields (currently used for phi1 and phi2)
h = 0.7
H_o = h*2.1331*(10.0**(-42.0)) #subscript 'o' denotes at current epoch, whereas 
Dc_o = (3.0/K**2.0)*(H_o**2.0)        #subscript '0' denotes initial conditions


