#!/usr/bin/env python
# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # #
# B A C K G R O U N D   E Q U A T I O N S #
# # # # # # # # # # # # # # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy / Dark Matter ODE solver

#This code contains the general evolution equations for background quantities
#which are passed to the ODE integrator.
import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin
from CONSTANTS import *


from MODEL import *

#Define all our equations

###############################
#####BACKGROUND EQUATIONS!#####
###############################


########################################################################
########################################################################
########################################################################

def H(In,t) : #define Friedmann eqn. Make sure all subsequent fns and 
                  #init conds have same ordering.
    Vin = V(In,t)
    VIin = VI(In,t)
    return (((K**2.0)/3.0)*(np.sum([In[0:(2+A)]]) + np.sum([(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1]**2.0)/2.0]) + Vin))**0.5 #usual!!!
    

def dH(In,t) : #define H dot - only needed in perturbed equations 
    Hin = H(In,t)    
    VPin = VP(In,t)
    dDM0in = dDM0(In,t)
    dDb0in = dDb0(In,t)
    dDr0in = dDr0(In,t)    
    dyin = dy(In,t)
    
    return ((K**2.0)/(6.0*(Hin)))*(np.sum(np.concatenate([[dDr0in,dDb0in],dDM0in])) + np.dot(In[(4+(A-1)+(I-1)):(4+(A-1)+2*(I-1)+1)],dyin) + np.dot(In[(4+(A-1)+(I-1)):(4+(A-1)+2*(I-1)+1)],VPin))


    

def DF0(In,t) : #This is the energy density of the field
    Vin = V(In,t)
    VIin = VI(In,t)
    return (In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1]**2.0)/2.0 + Vin



#######################################
####BACKGROUND INTEGRATED EQUATIONS####
#######################################

def dDr0(In,t) : #define rho rad dot eqn
    Hin = H(In,t)
    return -4.0*Hin*In[0] #the form of the fn, and return forces the output

def dDb0(In,t) : #define rho bary dot eqn
    Hin = H(In,t)
    return -3.0*Hin*In[1] #the form of the fn, and return forces the output

def dDM0(In,t) : #define rho DMA dot eqn
    Hin = H(In,t)
    return -3.0*Hin*(1.0+w[0:A])*In[2:2+A] -K*(np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],C[0:I,0:A]))*(1.0-3.0*w[0:A])*In[2:2+A] #the form of the fn, and return forces the output
    
def dx(In,t) : #define xI dot. 
       return In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1]
              
def dy(In,t) : #define phiI ddot eqn
    Hin = H(In,t)
    VPin = VP(In,t)
    return -3.0*Hin*In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1] - VPin[0:I] + K*np.dot(C[0:I,0:A],((1.0-3.0*w[0:A])*In[2:2+A]))    
       
