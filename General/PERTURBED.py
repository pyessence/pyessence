#!/usr/bin/env python
# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # 
# P E R T U R B E D   E Q U A T I O N S #
# # # # # # # # # # # # # # # # # # # # # 

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy / Dark Matter ODE solver

#This code contains the general perturbed evolution equations which are passed
#to the ODE integrator.
import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin

from BACKGROUND import *
from MODEL import *

#############################################
#####NON INTEGRATED PERTURBED EQUATIONS!#####
#############################################


#####Phi as a constraint, not integrated#########################

def X(In,t) : #Phi - metric potential
    VPin = VP(In,t)
    Hin = H(In,t)
    a = exp(t)
    return -((K**2.0)/(2.0*Hin))*(a*(np.dot(((1.0+w[0:A])*In[2:2+(A-1)+1]),In[10+2*(A-1)+2*(I-1):10+3*(A-1)+2*(I-1)+1])+In[1]*In[9+2*(A-1)+2*(I-1)]+4.0*In[0]*In[8+2*(A-1)+2*(I-1)]/3.0) - (np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1])))


def Y(In,t) : #B
    Hin = H(In,t)
    Xin = X(In,t)   
    a = exp(t)
    VPin = VP(In,t)
    dHin = dH(In,t)
    return (3.0*a)*((K**2.0)/(2.0))*(((1.0/(3.0*Hin))*(np.sum(In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]) + In[5+(A-1)+2*(I-1)] + In[6+(A-1)+2*(I-1)] - Xin*np.sum((In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1])**2.0) + np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1]) + np.dot(VPin[0:I],In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1]))) + np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1]) - a*(np.dot(((1.0+w[0:A])*In[2:2+(A-1)+1]),In[10+2*(A-1)+2*(I-1):10+3*(A-1)+2*(I-1)+1])+In[1]*In[9+2*(A-1)+2*(I-1)]+4.0*In[0]*In[8+2*(A-1)+2*(I-1)]/3.0))

#####Field density perturbations - calculated not integrated#####

def pDF0(In,t) : #This is the energy density of the field perturbations
    Hin = H(In,t)    
    VPin = VP(In,t)
    Xin = X(In,t) 
    return -Xin*(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1]**2.0) + In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1]*In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1] + In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1]*VPin[0:I]

#################################################################

#####Zeta - perturbation explosion check - not integrated########

def ZETA(In,t) :
    Hin = H(In,t)
    dDM0in = dDM0(In,t)
    dDr0in = dDr0(In,t)
    dDb0in = dDb0(In,t)
    Xin = X(In,t)
    return -Xin - Hin*(In[5+(A-1)+2*(I-1)] + In[6+(A-1)+2*(I-1)] + np.sum([In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]]))/(np.sum(dDM0in)+dDr0in+dDb0in)

#################################################################


##############################
#####PERTURBED EQUATIONS!#####
##############################

def dpDr0(In,t) : #delta rho b dot
    Hin = H(In,t)
    Xin = X(In,t)
    Yin = Y(In,t)
    a = exp(t)    
    return -4.0*Hin*(In[5+(A-1)+2*(I-1)]) + (4.0*((k**2.0)*(In[8+2*(A-1)+2*(I-1)]) - Yin)/(3.0*a))*(In[0])

def dpDb0(In,t) : #delta rho b dot     
    Hin = H(In,t)
    Xin = X(In,t)
    Yin = Y(In,t)
    a = exp(t)    
    return -3.0*Hin*(In[6+(A-1)+2*(I-1)]) + (((k**2.0)*(In[9+2*(A-1)+2*(I-1)]) - Yin)/a)*(In[1])

def dpDM0(In,t) : #delta rho 1 dot
    Hin = H(In,t)
    Xin = X(In,t)
    Yin = Y(In,t)
    a = exp(t)    
    return -3.0*Hin*(1.0+w[0:A])*(In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]) + (((k**2.0)*(In[10+2*(A-1)+2*(I-1):10+3*(A-1)+2*(I-1)+1])-Yin)/a)*(1.0+w[0:A])*(In[2:2+A]) - K*(1.0-3.0*w[0:A])*In[2:2+A]*np.dot(In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1],C[0:I,0:A]) - K*(1.0-3.0*w[0:A])*In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]*np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],C[0:I,0:A])

def dvr(In,t) : # vr dot
    Hin = H(In,t)
    dDr0in = dDr0(In,t)
    Xin = X(In,t)
    a = exp(t)
    return - Xin/a - In[5+(A-1)+2*(I-1)]/(4.0*In[0]*a)

def dvb(In,t) : # vb dot
    Hin = H(In,t)
    dDb0in = dDb0(In,t)
    Xin = X(In,t)
    a = exp(t)
    return - Hin*In[9+2*(A-1)+2*(I-1)] - Xin/a

def dv(In,t) : # v1 dot
    Hin = H(In,t)
    dDM0in = dDM0(In,t)
    Xin = X(In,t)
    a = exp(t)
    return (K*(1.0-3.0*w[0:A])*In[2:2+A]*np.dot(In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1],C[0:I,0:A]))/a + 3.0*w[0:A]*Hin*In[10+2*(A-1)+2*(I-1):10+3*(A-1)+2*(I-1)+1] - Hin*In[10+2*(A-1)+2*(I-1):10+3*(A-1)+2*(I-1)+1] - Xin/a - (w[0:A])*In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]/(a*(1.0+w[0:A])*In[2:2+A])

def dpx(In,t) : #phi1 dot equ, redundant since y1 could have just been used in 
            #np.array line below
    return In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1]       
    
def dpy(In,t) : # ddot phi1
    dHin = dH(In,t)
    Hin = H(In,t)
    VPin = VP(In,t)
    VPPin = VPP(In,t)
    Xin = X(In,t)
    Yin = Y(In,t)
    a = exp(t)
    return -3.0*Hin*In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1] - np.sum(np.dot(VPPin[0:I,0:I],In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1])) + (((K**2.0)/(2.0*Hin))*(np.sum(w[0:A]*In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]) + In[5+(A-1)+2*(I-1)]/3.0 - Xin*np.sum((In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1])**2.0) + np.dot(In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1],In[12+3*(A-1)+3*(I-1):12+3*(A-1)+4*(I-1)+1]) - np.dot(VPin[0:I],In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1])) -(3*(Hin**2.0) + 2.0*dHin)*Xin/Hin)*In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1] - ((k/a)**2.0)*In[11+3*(A-1)+2*(I-1):11+3*(A-1)+3*(I-1)+1] - (Yin*In[4+(A-1)+(I-1):4+(A-1)+2*(I-1)+1])/(a) - 2.0*VPin[0:I]*Xin + 2.0*K*np.dot(C[0:I,0:A],In[2:2+A])*Xin + 2.0*K*np.dot(C[0:I,0:A],((-3.0*w[0:A])*In[2:2+A]))*Xin + K*np.dot(C[0:I,0:A],In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]) + K*np.dot(C[0:I,0:A],((-3.0*w[0:A])*In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]))
    
#################THE ARRAY OF EQUATIONS PASSED TO INTEGRATOR#################

def df(t,In) : #this function constructs an array of our functions defined 
               #above to be fed to the first input in the ODE solver 
               #below

    Hin=H(In,t)    
    EQIN=np.concatenate([[dDr0(In,t),dDb0(In,t)],dDM0(In,t),dx(In,t),dy(In,t),[dpDr0(In,t),dpDb0(In,t)],dpDM0(In,t),[dvr(In,t),dvb(In,t)],dv(In,t),dpx(In,t),dpy(In,t)])
    return [EQIN]/Hin    


#############################################################################
