# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # #
# P L O T T E R #
# # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy ODE solver 

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin

from CONSTANTS import *
from MODEL import *
from BACKGROUND import *
from PERTURBED import *


data6 = np.load('EXAMPLE1dp5OUTPUTPAD14.npy')
data8 = np.load('EXAMPLE1dp5OUTPUTPAD8.npy')
data10 = np.load('EXAMPLE1dp5OUTPUTPAD10.npy')
data12 = np.load('EXAMPLE1dp5OUTPUTPAD12.npy')

longphi8 = np.load('LONGPHIPAD8.npy')
longphi10 = np.load('LONGPHIPAD10.npy')
longphi12 = np.load('LONGPHIPAD12.npy')
longphi14 = np.load('LONGPHIPAD14.npy')



t_out = np.load('EXAMPLE1dp5TIMEPAD8.npy')

##############Log plots section! -start- #################

####Background####

log_data6 = log10(((data6)**2.0)**0.5)
log_data8 = log10(((data8)**2.0)**0.5)
log_data10 = log10(((data10)**2.0)**0.5)
log_data12 = log10(((data12)**2.0)**0.5)


log_bandrResults6 = log10(data6[:,1]+data6[:,0])
log_bandrResults8 = log10(data8[:,1]+data8[:,0])
log_bandrResults10 = log10(data10[:,1]+data10[:,0])
log_bandrResults12 = log10(data12[:,1]+data12[:,0])

plt.plot(t_out,log_bandrResults6,label=r'$(b_\rho + r_\rho )$14')
plt.plot(t_out,log_bandrResults8,label=r'$(b_\rho + r_\rho )$8')
plt.plot(t_out,log_bandrResults10,label=r'$(b_\rho + r_\rho )$10')
plt.plot(t_out,log_bandrResults12,label=r'$(b_\rho + r_\rho )$12')

plt.title('Background Densities')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
#p.ylim([-55,-25])
#p.xlim([-12,-11])

plt.legend()
plt.show()

####Perturbations####


abs_log_fulloutput96 = log10(((data6[:,3])**2.0)**0.5)
abs_log_fulloutput86 = log10(((data6[:,2])**2.0)**0.5)

abs_log_fulloutput98 = log10(((data8[:,3])**2.0)**0.5)
abs_log_fulloutput88 = log10(((data8[:,2])**2.0)**0.5)

abs_log_fulloutput910 = log10(((data10[:,3])**2.0)**0.5)
abs_log_fulloutput810 = log10(((data10[:,2])**2.0)**0.5)

abs_log_fulloutput912 = log10(((data12[:,3])**2.0)**0.5)
abs_log_fulloutput812 = log10(((data12[:,2])**2.0)**0.5)


plt.plot(t_out,abs_log_fulloutput96,label=r'$\delta \rho (Db)$14')
plt.plot(t_out,abs_log_fulloutput86,label=r'$\delta \rho (Dr)$14')

plt.plot(t_out,abs_log_fulloutput98,label=r'$\delta \rho (Db)$8')
plt.plot(t_out,abs_log_fulloutput88,label=r'$\delta \rho (Dr)$8')

plt.plot(t_out,abs_log_fulloutput910,label=r'$\delta \rho (Db)$10')
plt.plot(t_out,abs_log_fulloutput810,label=r'$\delta \rho (Dr)$10')

plt.plot(t_out,abs_log_fulloutput912,label=r'$\delta \rho (Db)$12')
plt.plot(t_out,abs_log_fulloutput812,label=r'$\delta \rho (Dr)$12')


plt.title('Dark Perturbations')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()


####Density Contrast####

PHIi=(kvko*H_o/keq)**(-3.0/2.0)

dcon96 = (data6[:,3]/data6[:,1])
dcon86 = (data6[:,2]/data6[:,0])
kkodcon96=-((PHIi)**(-1.0))*dcon96
kkodcon86=-((PHIi)**(-1.0))*dcon86
plt.plot(t_out,kkodcon96,label=r'$\delta (Db)$14')
plt.plot(t_out,kkodcon86,label=r'$\delta (Dr)$14')

dcon98 = (data8[:,3]/data8[:,1])
dcon88 = (data8[:,2]/data8[:,0])
kkodcon98=-((PHIi)**(-1.0))*dcon98
kkodcon88=-((PHIi)**(-1.0))*dcon88
plt.plot(t_out,kkodcon98,label=r'$\delta (Db)$8')
plt.plot(t_out,kkodcon88,label=r'$\delta (Dr)$8')

dcon910 = (data10[:,3]/data10[:,1])
dcon810 = (data10[:,2]/data10[:,0])
kkodcon910=-((PHIi)**(-1.0))*dcon910
kkodcon810=-((PHIi)**(-1.0))*dcon810
plt.plot(t_out,kkodcon910,label=r'$\delta (Db)$10')
plt.plot(t_out,kkodcon810,label=r'$\delta (Dr)$10')

dcon912 = (data12[:,3]/data12[:,1])
dcon812 = (data12[:,2]/data12[:,0])
kkodcon912=-((PHIi)**(-1.0))*dcon912
kkodcon812=-((PHIi)**(-1.0))*dcon812
plt.plot(t_out,kkodcon912,label=r'$\delta (Db)$12')
plt.plot(t_out,kkodcon812,label=r'$\delta (Dr)$12')

plt.title('-((PHIi)**(-1.0)) Dens cont mad and rad')
plt.xlabel('e-folds')
plt.ylabel(' dens cont')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####sigma####
dcon96 = (data6[:,3]/data6[:,1])
dcon86 = (data6[:,2]/data6[:,0])
kkodcon96=-((PHIi)**(-1.0))*dcon96
kkodcon86=-((PHIi)**(-1.0))*dcon86

dcon98 = (data8[:,3]/data8[:,1])
dcon88 = (data8[:,2]/data8[:,0])
kkodcon98=-((PHIi)**(-1.0))*dcon98
kkodcon88=-((PHIi)**(-1.0))*dcon88

dcon910 = (data10[:,3]/data10[:,1])
dcon810 = (data10[:,2]/data10[:,0])
kkodcon910=-((PHIi)**(-1.0))*dcon910
kkodcon810=-((PHIi)**(-1.0))*dcon810

dcon912 = (data12[:,3]/data12[:,1])
dcon812 = (data12[:,2]/data12[:,0])
kkodcon912=-((PHIi)**(-1.0))*dcon912
kkodcon812=-((PHIi)**(-1.0))*dcon812

sigma6=(3.0/4.0)*dcon86 - dcon96
sigma8=(3.0/4.0)*dcon88 - dcon98
sigma10=(3.0/4.0)*dcon810 - dcon910
sigma12=(3.0/4.0)*dcon812 - dcon912


plt.plot(t_out,log10(1.0+sigma6),label=r'$\sigma$14')
plt.plot(t_out,log10(1.0+sigma8),label=r'$\sigma$8')
plt.plot(t_out,log10(1.0+sigma10),label=r'$\sigma$10')
plt.plot(t_out,log10(1.0+sigma12),label=r'$\sigma$12')

plt.title('sigma')
plt.xlabel('e-folds')
plt.ylabel('log_10(1+sigma)')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

###v1###
###v2###

log_fulloutput6 = log10(((data6)**2.0)**0.5)
log_fulloutput8 = log10(((data8)**2.0)**0.5)
log_fulloutput10 = log10(((data10)**2.0)**0.5)
log_fulloutput12 = log10(((data12)**2.0)**0.5)

#

plt.plot(t_out,log_fulloutput6[:,5],label='vb14')
plt.plot(t_out,log_fulloutput6[:,4],label='vr14')
plt.plot(t_out,log_fulloutput8[:,5],label='vb8')
plt.plot(t_out,log_fulloutput8[:,4],label='vr8')
plt.plot(t_out,log_fulloutput10[:,5],label='vb10')
plt.plot(t_out,log_fulloutput10[:,4],label='vr10')
plt.plot(t_out,log_fulloutput12[:,5],label='vb12')
plt.plot(t_out,log_fulloutput12[:,4],label='vr12')


plt.title('3-velocities')
plt.xlabel('e-folds')
plt.ylabel('log velocity')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()


####ZETA####

tshape = t_out.shape
print tshape
data6shape = data6.shape
print data6shape

for x in range(0,tshape[0]):
    
    if x==0 : 
        ZETARESULTS6 = ZETA(data6[x,:],t_out[x])
        ZETARESULTS8 = ZETA(data8[x,:],t_out[x])
        ZETARESULTS10 = ZETA(data10[x,:],t_out[x])
        ZETARESULTS12 = ZETA(data12[x,:],t_out[x])
    else:
        Z6i = ZETA(data6[x,:],t_out[x])
        ZETARESULTS6 = np.vstack([ZETARESULTS6,Z6i])
        Z8i = ZETA(data8[x,:],t_out[x])
        ZETARESULTS8 = np.vstack([ZETARESULTS8,Z8i])
        Z10i = ZETA(data10[x,:],t_out[x])
        ZETARESULTS10 = np.vstack([ZETARESULTS10,Z10i])
        Z12i = ZETA(data12[x,:],t_out[x])        
        ZETARESULTS12 = np.vstack([ZETARESULTS12,Z12i])


log_ZETAResults6 = log10(((ZETARESULTS6)**2.0)**0.5)
log_ZETAResults8 = log10(((ZETARESULTS8)**2.0)**0.5)
log_ZETAResults10 = log10(((ZETARESULTS10)**2.0)**0.5)
log_ZETAResults12 = log10(((ZETARESULTS12)**2.0)**0.5)

plt.plot(t_out,log_ZETAResults6,label=r'$\zeta$14')
plt.plot(t_out,log_ZETAResults8,label=r'$\zeta$8')
plt.plot(t_out,log_ZETAResults10,label=r'$\zeta$10')
plt.plot(t_out,log_ZETAResults12,label=r'$\zeta$12')



plt.title('Zeta')
plt.xlabel('e-folds')
plt.ylabel('log Zeta')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()


#################################################
###All remaining values##########################
#################################################

###Phi###

for x in range(0,tshape[0]):
    
    if x==0 : 
        PhiResults6 = X(data6[x,:],t_out[x])
        PhiResults8 = X(data8[x,:],t_out[x])
        PhiResults10 = X(data10[x,:],t_out[x])
        PhiResults12 = X(data12[x,:],t_out[x])
    else:
        Z6i = X(data6[x,:],t_out[x])
        PhiResults6 = np.vstack([PhiResults6,Z6i])
        Z8i = X(data8[x,:],t_out[x])
        PhiResults8 = np.vstack([PhiResults8,Z8i])
        Z10i = X(data10[x,:],t_out[x])
        PhiResults10 = np.vstack([PhiResults10,Z10i])
        Z12i = X(data12[x,:],t_out[x])        
        PhiResults12 = np.vstack([PhiResults12,Z12i])


log_PhiResults6 = log10(((PhiResults6)**2.0)**0.5)
log_PhiResults8 = log10(((PhiResults8)**2.0)**0.5)
log_PhiResults10 = log10(((PhiResults10)**2.0)**0.5)
log_PhiResults12 = log10(((PhiResults12)**2.0)**0.5)

kkoPhiResults6 = ((PHIi)**(-1.0))*(((PhiResults6)**2.0)**0.5)
kkoPhiResults8 = ((PHIi)**(-1.0))*(((PhiResults8)**2.0)**0.5)
kkoPhiResults10 = ((PHIi)**(-1.0))*(((PhiResults10)**2.0)**0.5)
kkoPhiResults12 = ((PHIi)**(-1.0))*(((PhiResults12)**2.0)**0.5)

#
plt.plot(t_out,kkoPhiResults6,label=r'((PHIi)**(-1.0)) $\Phi$14')
plt.plot(t_out,kkoPhiResults8,label=r'((PHIi)**(-1.0)) $\Phi$8')
plt.plot(t_out,kkoPhiResults10,label=r'((PHIi)**(-1.0)) $\Phi$10')
plt.plot(t_out,kkoPhiResults12,label=r'((PHIi)**(-1.0)) $\Phi$12')

plt.title('$\Phi$')
plt.xlabel('e-folds')
plt.ylabel('((PHIi)**(-1.0)) $\Phi$')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

######long phi########

tshape = t_out.shape


for x in range(0,tshape[0]):
    
    if x==0 : 
        HResults6 = H(data6[x,:],t_out[x])
        BResults6 = Y(data6[x,:],t_out[x])
        aResults = exp(x)
        kkoLongPhiResults6 = ((PHIi)**(-1.0))*(((-HResults6*BResults6*aResults/(k**2.0))**2.0)**0.5)
    else:
        HR6 = H(data6[x,:],t_out[x])
        HResults6 = np.vstack([HResults6,HR6])
        BR6 = Y(data6[x,:],t_out[x])
        BResults6 = np.vstack([BResults6,BR6])
        aR = exp(x)
        aResults = np.vstack([aResults,aR])
        kk6 = ((PHIi)**(-1.0))*(((-HR6*BR6*aR/(k**2.0))**2.0)**0.5)
        kkoLongPhiResults6 = np.vstack([kkoLongPhiResults6,kk6])

for x in range(0,tshape[0]):
    
    if x==0 : 
        HResults8 = H(data8[x,:],t_out[x])
        BResults8 = Y(data8[x,:],t_out[x])
        aResults = exp(x)
        kkoLongPhiResults8 = ((PHIi)**(-1.0))*(((-HResults8*BResults8*aResults/(k**2.0))**2.0)**0.5)
    else:
        HR8 = H(data8[x,:],t_out[x])
        HResults8 = np.vstack([HResults8,HR8])
        BR8 = Y(data8[x,:],t_out[x])
        BResults8 = np.vstack([BResults8,BR8])
        aR = exp(x)
        aResults = np.vstack([aResults,aR])
        kk8 = ((PHIi)**(-1.0))*(((-HR8*BR8*aR/(k**2.0))**2.0)**0.5)
        kkoLongPhiResults8 = np.vstack([kkoLongPhiResults8,kk8])

for x in range(0,tshape[0]):
    
    if x==0 : 
        HResults10 = H(data10[x,:],t_out[x])
        BResults10 = Y(data10[x,:],t_out[x])
        aResults = exp(x)
        kkoLongPhiResults10 = ((PHIi)**(-1.0))*(((-HResults10*BResults10*aResults/(k**2.0))**2.0)**0.5)
    else:
        HR10 = H(data10[x,:],t_out[x])
        HResults10 = np.vstack([HResults10,HR10])
        BR10 = Y(data10[x,:],t_out[x])
        BResults10 = np.vstack([BResults10,BR10])
        aR = exp(x)
        aResults = np.vstack([aResults,aR])
        kk10 = ((PHIi)**(-1.0))*(((-HR10*BR10*aR/(k**2.0))**2.0)**0.5)
        kkoLongPhiResults10 = np.vstack([kkoLongPhiResults10,kk10])


for x in range(0,tshape[0]):
    
    if x==0 : 
        HResults12 = H(data12[x,:],t_out[x])
        BResults12 = Y(data12[x,:],t_out[x])
        aResults = exp(x)
        kkoLongPhiResults12 = ((PHIi)**(-1.0))*(((-HResults12*BResults12*aResults/(k**2.0))**2.0)**0.5)
    else:
        HR12 = H(data12[x,:],t_out[x])
        HResults12 = np.vstack([HResults12,HR12])
        BR12 = Y(data12[x,:],t_out[x])
        BResults12 = np.vstack([BResults12,BR12])
        aR = exp(x)
        aResults = np.vstack([aResults,aR])
        kk12 = ((PHIi)**(-1.0))*(((-HR12*BR12*aR/(k**2.0))**2.0)**0.5)
        kkoLongPhiResults12 = np.vstack([kkoLongPhiResults12,kk12])


plt.plot(t_out,longphi14,label=r'((PHIi)**(-1.0)) $\Phi$14 Long')
plt.plot(t_out,longphi8,label=r'((PHIi)**(-1.0)) $\Phi$8 Long')
plt.plot(t_out,longphi10,label=r'((PHIi)**(-1.0)) $\Phi$10 Long')
plt.plot(t_out,longphi12,label=r'((PHIi)**(-1.0)) $\Phi$12 Long')

plt.title('Long $\Phi$')
plt.xlabel('e-folds')
plt.ylabel('((PHIi)**(-1.0)) $\Phi$')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

##############Log plots section! -end- #################
