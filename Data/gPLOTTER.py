# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# E X A M P L E   1   -   A   S I N G L E   R U N   W I T H               #
# O N E   S E T   O F   C O U P L I N G S   T O                           #
# D E M O N S T R A T I N G   P L O T T I N G   O F   P A R A M E T E R S #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

###########################################################################
###########################################################################
#R E M E M B E R  T O  S E T  ks  I N  E A C H  F U N C T I O N ! ! ! ! ! #
###########################################################################
###########################################################################

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin

from CONSTANTS import *
from MODEL import *
from BACKGROUND import *
from PERTURBED import *

data = np.load('2FL2FIOUTPUTFullAdk2.npy')
data2 = np.load('2FL2FIOUTPUTFullAdk10.npy')
data3 = np.load('2FL2FIOUTPUTFullAdk50.npy')
data4 = np.load('2FL2FIOUTPUTFullAdk100.npy')
data5 = np.load('2FL2FIOUTPUTFullAdk200.npy')
data6 = np.load('2FL2FIOUTPUTFullAdk300.npy')
data7 = np.load('2FL2FIOUTPUTFullAdk400.npy')
data8 = np.load('2FL2FIOUTPUTFullAdk500.npy')
data9 = np.load('2FL2FIOUTPUTFullAdk600.npy')
data10 = np.load('2FL2FIOUTPUTFullAdk700.npy')
data11 = np.load('2FL2FIOUTPUTFullAdk800.npy')
data12 = np.load('2FL2FIOUTPUTFullAdk900.npy')
data13 = np.load('2FL2FIOUTPUTFullAdk1000.npy')


t_out = np.load('2FL2FITIMEFullAdk2.npy')

####################Times############################
t_i=log(10.0**(-6.0));t_f=log(10.0**(0.5));step=0.01
#####################################################


tshape = t_out.shape
print tshape
now=-int(t_i/step) + 1 
print now

datanow=data[now,:]
data2now=data2[now,:]
data3now=data3[now,:]
data4now=data4[now,:]
data5now=data5[now,:]
data6now=data6[now,:]
data7now=data7[now,:]
data8now=data8[now,:]
data9now=data9[now,:]
data10now=data10[now,:]
data11now=data11[now,:]
data12now=data12[now,:]
data13now=data13[now,:]

tnow=t_out[now]

delta0=(np.sum(((datanow[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(datanow[2:2+(A-1)+1])))
delta02=(np.sum(((data2now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data2now[2:2+(A-1)+1])))
delta03=(np.sum(((data3now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data3now[2:2+(A-1)+1])))
delta04=(np.sum(((data4now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data4now[2:2+(A-1)+1])))
delta05=(np.sum(((data5now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data5now[2:2+(A-1)+1])))
delta06=(np.sum(((data6now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data6now[2:2+(A-1)+1])))
delta07=(np.sum(((data7now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data7now[2:2+(A-1)+1])))
delta08=(np.sum(((data8now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data8now[2:2+(A-1)+1])))
delta09=(np.sum(((data9now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data9now[2:2+(A-1)+1])))
delta010=(np.sum(((data10now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data10now[2:2+(A-1)+1])))
delta011=(np.sum(((data11now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data11now[2:2+(A-1)+1])))
delta012=(np.sum(((data12now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data12now[2:2+(A-1)+1])))
delta013=(np.sum(((data13now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(data13now[2:2+(A-1)+1])))

def growth(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 2.0*H_o   
    return (1.0/(delta0))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth2(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 10.0*H_o
    return (1.0/(delta02))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth3(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 50.0*H_o   
    return (1.0/(delta03))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth4(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 100.0*H_o   
    return (1.0/(delta04))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth5(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 200.0*H_o   
    return (1.0/(delta05))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth6(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 300.0*H_o   
    return (1.0/(delta06))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth7(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 400.0*H_o
    return (1.0/(delta07))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth8(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 500.0*H_o   
    return (1.0/(delta08))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth9(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 600.0*H_o   
    return (1.0/(delta09))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth10(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 700.0*H_o   
    return (1.0/(delta010))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth11(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 800.0*H_o   
    return (1.0/(delta011))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth12(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 900.0*H_o
    return (1.0/(delta012))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))

def growth13(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 1000.0*H_o   
    return (1.0/(delta013))*(np.sum(((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1]**2.0)**0.5)/(In[2:2+(A-1)+1])))
    

for x in range(0,tshape[0]):
    
    if x==0 : 
        GrowthResults = growth(data[x,:],t_out[x])
        Growth2Results = growth2(data2[x,:],t_out[x])
        Growth3Results = growth3(data3[x,:],t_out[x])
        Growth4Results = growth4(data4[x,:],t_out[x])
        Growth5Results = growth5(data5[x,:],t_out[x])
        Growth6Results = growth6(data6[x,:],t_out[x])
        Growth7Results = growth7(data7[x,:],t_out[x])
        Growth8Results = growth8(data8[x,:],t_out[x])
        Growth9Results = growth9(data9[x,:],t_out[x])
        Growth10Results = growth10(data10[x,:],t_out[x])
        Growth11Results = growth11(data11[x,:],t_out[x])
        Growth12Results = growth12(data12[x,:],t_out[x])
        Growth13Results = growth13(data13[x,:],t_out[x])
    else:
        Gi=growth(data[x,:],t_out[x])
        G2i=growth2(data2[x,:],t_out[x])
        G3i=growth3(data3[x,:],t_out[x])
        G4i=growth4(data4[x,:],t_out[x])
        G5i=growth5(data5[x,:],t_out[x])
        G6i = growth6(data6[x,:],t_out[x])
        G7i = growth7(data7[x,:],t_out[x])
        G8i = growth8(data8[x,:],t_out[x])
        G9i = growth9(data9[x,:],t_out[x])
        G10i = growth10(data10[x,:],t_out[x])
        G11i = growth11(data11[x,:],t_out[x])
        G12i = growth12(data12[x,:],t_out[x])
        G13i = growth13(data13[x,:],t_out[x])
        GrowthResults = np.vstack([GrowthResults,Gi])
        Growth2Results = np.vstack([Growth2Results,G2i])
        Growth3Results = np.vstack([Growth3Results,G3i])
        Growth4Results = np.vstack([Growth4Results,G4i])
        Growth5Results = np.vstack([Growth5Results,G5i])
        Growth6Results = np.vstack([Growth6Results,G6i])
        Growth7Results = np.vstack([Growth7Results,G7i])
        Growth8Results = np.vstack([Growth8Results,G8i])
        Growth9Results = np.vstack([Growth9Results,G9i])
        Growth10Results = np.vstack([Growth10Results,G10i])
        Growth11Results = np.vstack([Growth11Results,G11i])
        Growth12Results = np.vstack([Growth12Results,G12i])
        Growth13Results = np.vstack([Growth13Results,G13i])

####Growth Factor####

#
plt.plot(t_out,GrowthResults,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=2H_o')
plt.plot(t_out,Growth2Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=10H_o')
plt.plot(t_out,Growth3Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=50H_o')
plt.plot(t_out,Growth4Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=100H_o')
plt.plot(t_out,Growth5Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=200H_o')
plt.plot(t_out,Growth6Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=300H_o')
plt.plot(t_out,Growth7Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=400H_o')
plt.plot(t_out,Growth8Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=500H_o',color='0.40')
plt.plot(t_out,Growth9Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=600H_o',color='0.50')
plt.plot(t_out,Growth10Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=700H_o',color='0.60')
plt.plot(t_out,Growth11Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=800H_o',color='0.70')
plt.plot(t_out,Growth12Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=900H_o',color='0.80')
plt.plot(t_out,Growth13Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=1000H_o',color='0.90')

plt.title(r'Growth, g, $\frac{\delta}{\delta_0}$ for 2 CDM fluids, 2 Scalar Fields - flat')
plt.xlabel('e-folds')
plt.ylabel('g')
#
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

####Log Growth Factor####

#
plt.plot(t_out,log10(GrowthResults),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=2H_o')
plt.plot(t_out,log10(Growth2Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=10H_o')
plt.plot(t_out,log10(Growth3Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=50H_o')
plt.plot(t_out,log10(Growth4Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=100H_o')
plt.plot(t_out,log10(Growth5Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=200H_o')
plt.plot(t_out,log10(Growth6Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=300H_o')
plt.plot(t_out,log10(Growth7Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=400H_o')
plt.plot(t_out,log10(Growth8Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=500H_o',color='0.40')
plt.plot(t_out,log10(Growth9Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=600H_o',color='0.50')
plt.plot(t_out,log10(Growth10Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=700H_o',color='0.60')
plt.plot(t_out,log10(Growth11Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=800H_o',color='0.70')
plt.plot(t_out,log10(Growth12Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=900H_o',color='0.80')
plt.plot(t_out,log10(Growth13Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=1000H_o',color='0.90')

plt.title(r'Log Growth, g, $\frac{\delta}{\delta_0}$ for 2 CDM fluids, 2 Scalar Fields - flat')
plt.xlabel('e-folds')
plt.ylabel('g')
#

##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()
