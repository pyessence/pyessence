# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # #
# P L O T T E R #
# # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy ODE solver 

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin

from CONSTANTS import *
from MODEL import *
from BACKGROUND import *
from PERTURBED import *

#data6 = np.load('EXAMPLE1OUTPUTdp56.npy')
data8 = np.load('EXAMPLE1OUTPUTdp58.npy')
data10 = np.load('EXAMPLE1OUTPUTdp510.npy')
data12 = np.load('EXAMPLE1OUTPUTdp512.npy')
data14 = np.load('EXAMPLE1OUTPUTdp514.npy')

t_out = np.load('EXAMPLE1TIMEdp58.npy')

tshape = t_out.shape
print tshape
data8shape = data8.shape
print data8shape

##############Log plots section! -start- #################

####Background####

#log_data6 = log10(((data6)**2.0)**0.5)
log_data8 = log10(((data8)**2.0)**0.5)
log_data10 = log10(((data10)**2.0)**0.5)
log_data12 = log10(((data12)**2.0)**0.5)
log_data14 = log10(((data14)**2.0)**0.5)

#log_bandrResults6 = log10(data6[:,1]+data6[:,0])
log_bandrResults8 = log10(data8[:,1]+data8[:,0])
log_bandrResults10 = log10(data10[:,1]+data10[:,0])
log_bandrResults12 = log10(data12[:,1]+data12[:,0])
log_bandrResults14 = log10(data14[:,1]+data14[:,0])

#plt.plot(t_out,log_data6[:,2],label=r'DM1 $\rho$6')
#plt.plot(t_out,log_data6[:,3],label=r'DM2 $\rho$6')
plt.plot(t_out,log_data8[:,2],label=r'DM1 $\rho$8')
plt.plot(t_out,log_data8[:,3],label=r'DM2 $\rho$8')
plt.plot(t_out,log_data10[:,2],label=r'DM1 $\rho$10')
plt.plot(t_out,log_data10[:,3],label=r'DM2 $\rho$10')
plt.plot(t_out,log_data12[:,2],label=r'DM1 $\rho$12')
plt.plot(t_out,log_data12[:,3],label=r'DM2 $\rho$12')
plt.plot(t_out,log_data14[:,2],label=r'DM1 $\rho$14')
plt.plot(t_out,log_data14[:,3],label=r'DM2 $\rho$14')

for x in range(0,tshape[0]):
    
    if x==0 : 
#        DFresults6 = DF0(data6[x,:],t_out[x])
#        DF0Results6 = DFresults6[0]
#        DF20Results6 = DFresults6[1]
        DFresults8 = DF0(data8[x,:],t_out[x])
        DF0Results8 = DFresults8[0]
        DF20Results8 = DFresults8[1]
        DFresults10 = DF0(data10[x,:],t_out[x])
        DF0Results10 = DFresults10[0]
        DF20Results10 = DFresults10[1]
        DFresults12 = DF0(data12[x,:],t_out[x])
        DF0Results12 = DFresults12[0]
        DF20Results12 = DFresults12[1]
        DFresults14 = DF0(data14[x,:],t_out[x])
        DF0Results14 = DFresults14[0]
        DF20Results14 = DFresults14[1]
    else:
#        D6i = DF0(data6[x,:],t_out[x])
#        DF0Results6 = np.vstack([DF0Results6,D6i[0]])
#        DF20Results6 = np.vstack([DF20Results6,D6i[1]])
        D8i = DF0(data8[x,:],t_out[x])
        DF0Results8 = np.vstack([DF0Results8,D8i[0]])
        DF20Results8 = np.vstack([DF20Results8,D8i[1]])
        D10i = DF0(data10[x,:],t_out[x])
        DF0Results10 = np.vstack([DF0Results10,D10i[0]])
        DF20Results10 = np.vstack([DF20Results10,D10i[1]])
        D12i = DF0(data12[x,:],t_out[x])
        DF0Results12 = np.vstack([DF0Results12,D12i[0]])
        DF20Results12 = np.vstack([DF20Results12,D12i[1]])
        D14i = DF0(data14[x,:],t_out[x])
        DF0Results14 = np.vstack([DF0Results14,D14i[0]])
        DF20Results14 = np.vstack([DF20Results14,D14i[1]])

#log_DF0Results6=log10(((DF0Results6)**2.0)**0.5)
log_DF0Results8=log10(((DF0Results8)**2.0)**0.5)
log_DF0Results10=log10(((DF0Results10)**2.0)**0.5)
log_DF0Results12=log10(((DF0Results12)**2.0)**0.5)
log_DF0Results14=log10(((DF0Results14)**2.0)**0.5)
#log_DF20Results6=log10(((DF20Results6)**2.0)**0.5)
log_DF20Results8=log10(((DF20Results8)**2.0)**0.5)
log_DF20Results10=log10(((DF20Results10)**2.0)**0.5)
log_DF20Results12=log10(((DF20Results12)**2.0)**0.5)
log_DF20Results14=log10(((DF20Results14)**2.0)**0.5)


#plt.plot(t_out,log_DF0Results6,label=r'$\phi_1 \rho$6')
#plt.plot(t_out,log_DF20Results6,label=r'$\phi_2 \rho$6')
plt.plot(t_out,log_DF0Results8,label=r'$\phi_1 \rho$8')
plt.plot(t_out,log_DF20Results8,label=r'$\phi_2 \rho$8')
plt.plot(t_out,log_DF0Results10,label=r'$\phi_1 \rho$10')
plt.plot(t_out,log_DF20Results10,label=r'$\phi_2 \rho$10')
plt.plot(t_out,log_DF0Results12,label=r'$\phi_1 \rho$12')
plt.plot(t_out,log_DF20Results12,label=r'$\phi_2 \rho$12')
plt.plot(t_out,log_DF0Results14,label=r'$\phi_1 \rho$14')
plt.plot(t_out,log_DF20Results14,label=r'$\phi_2 \rho$14')


#plt.plot(t_out,log_bandrResults6,label=r'$(b_\rho + r_\rho )$6')
plt.plot(t_out,log_bandrResults8,label=r'$(b_\rho + r_\rho )$8')
plt.plot(t_out,log_bandrResults10,label=r'$(b_\rho + r_\rho )$10')
plt.plot(t_out,log_bandrResults12,label=r'$(b_\rho + r_\rho )$12')
plt.plot(t_out,log_bandrResults12,label=r'$(b_\rho + r_\rho )$14')


plt.title('Background Densities')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
#p.ylim([-55,-25])
#p.xlim([-12,-11])

plt.legend()
plt.show()

####Perturbations - "Matter"####


#abs_log_fulloutput96 = log10(((data6[:,3])**2.0)**0.5)
#abs_log_fulloutput86 = log10(((data6[:,2])**2.0)**0.5)
#abs_log_fulloutput106 = log10(((data6[:,10])**2.0)**0.5)
#abs_log_fulloutput116 = log10(((data6[:,11])**2.0)**0.5)

abs_log_fulloutput98 = log10(((data8[:,3])**2.0)**0.5)
abs_log_fulloutput88 = log10(((data8[:,2])**2.0)**0.5)
abs_log_fulloutput108 = log10(((data8[:,10])**2.0)**0.5)
abs_log_fulloutput118 = log10(((data8[:,11])**2.0)**0.5)

abs_log_fulloutput910 = log10(((data10[:,3])**2.0)**0.5)
abs_log_fulloutput810 = log10(((data10[:,2])**2.0)**0.5)
abs_log_fulloutput1010 = log10(((data10[:,10])**2.0)**0.5)
abs_log_fulloutput1110 = log10(((data10[:,11])**2.0)**0.5)

abs_log_fulloutput912 = log10(((data12[:,3])**2.0)**0.5)
abs_log_fulloutput812 = log10(((data12[:,2])**2.0)**0.5)
abs_log_fulloutput1012 = log10(((data12[:,10])**2.0)**0.5)
abs_log_fulloutput1112 = log10(((data12[:,11])**2.0)**0.5)

abs_log_fulloutput914 = log10(((data14[:,3])**2.0)**0.5)
abs_log_fulloutput814 = log10(((data14[:,2])**2.0)**0.5)
abs_log_fulloutput1014 = log10(((data14[:,10])**2.0)**0.5)
abs_log_fulloutput1114 = log10(((data14[:,11])**2.0)**0.5)

for x in range(0,tshape[0]):
    
    if x==0 : 
#        pDFresults6 = pDF0(data6[x,:],t_out[x])
#        pDF0Results6 = pDFresults6[0]
#        pDF20Results6 = pDFresults6[1]
        pDFresults8 = pDF0(data8[x,:],t_out[x])
        pDF0Results8 = pDFresults8[0]
        pDF20Results8 = pDFresults8[1]
        pDFresults10 = pDF0(data10[x,:],t_out[x])
        pDF0Results10 = pDFresults10[0]
        pDF20Results10 = pDFresults10[1]
    else:
#        pD6i = pDF0(data6[x,:],t_out[x])
#        pDF0Results6 = np.vstack([pDF0Results6,pD6i[0]])
#        pDF20Results6 = np.vstack([pDF20Results6,pD6i[1]])
        pD8i = pDF0(data8[x,:],t_out[x])
        pDF0Results8 = np.vstack([pDF0Results8,pD8i[0]])
        pDF20Results8 = np.vstack([pDF20Results8,pD8i[1]])
        pD10i = pDF0(data10[x,:],t_out[x])
        pDF0Results10 = np.vstack([pDF0Results10,pD10i[0]])
        pDF20Results10 = np.vstack([pDF20Results10,pD10i[1]])



#log_pDF0Results6=log10(((pDF0Results6)**2.0)**0.5)
log_pDF0Results8=log10(((pDF0Results8)**2.0)**0.5)
log_pDF0Results10=log10(((pDF0Results10)**2.0)**0.5)
#log_pDF20Results6=log10(((pDF20Results6)**2.0)**0.5)
log_pDF20Results8=log10(((pDF20Results8)**2.0)**0.5)
log_pDF20Results10=log10(((pDF20Results10)**2.0)**0.5)


#plt.plot(t_out,abs_log_fulloutput96,label=r'$\delta \rho (Db)$6')
#plt.plot(t_out,abs_log_fulloutput86,label=r'$\delta \rho (Dr)$6')
#plt.plot(t_out,abs_log_fulloutput106,label=r'$\delta \rho (DM1)$6')
#plt.plot(t_out,abs_log_fulloutput116,label=r'$\delta \rho (DM2)$6')
#plt.plot(t_out,log_pDF0Results6,label=r'$\delta \rho_{\phi_1}$6')
#plt.plot(t_out,log_pDF20Results6,label=r'$\delta \rho_{\phi_2}$6')

plt.plot(t_out,abs_log_fulloutput98,label=r'$\delta \rho (Db)$8')
plt.plot(t_out,abs_log_fulloutput88,label=r'$\delta \rho (Dr)$8')
plt.plot(t_out,abs_log_fulloutput108,label=r'$\delta \rho (DM1)$8')
plt.plot(t_out,abs_log_fulloutput118,label=r'$\delta \rho (DM2)$8')
#plt.plot(t_out,log_pDF0Results8,label=r'$\delta \rho_{\phi_1}$8')
#plt.plot(t_out,log_pDF20Results8,label=r'$\delta \rho_{\phi_2}$8')

plt.plot(t_out,abs_log_fulloutput910,label=r'$\delta \rho (Db)$10')
plt.plot(t_out,abs_log_fulloutput810,label=r'$\delta \rho (Dr)$10')
plt.plot(t_out,abs_log_fulloutput1010,label=r'$\delta \rho (DM1)$10')
plt.plot(t_out,abs_log_fulloutput1110,label=r'$\delta \rho (DM2)$10')
#plt.plot(t_out,log_pDF0Results10,label=r'$\delta \rho_{\phi_1}$10')
#plt.plot(t_out,log_pDF20Results10,label=r'$\delta \rho_{\phi_2}$10')

plt.plot(t_out,abs_log_fulloutput912,label=r'$\delta \rho (Db)$12')
plt.plot(t_out,abs_log_fulloutput812,label=r'$\delta \rho (Dr)$12')
plt.plot(t_out,abs_log_fulloutput1012,label=r'$\delta \rho (DM1)$12')
plt.plot(t_out,abs_log_fulloutput1112,label=r'$\delta \rho (DM2)$12')

plt.plot(t_out,abs_log_fulloutput914,label=r'$\delta \rho (Db)$14')
plt.plot(t_out,abs_log_fulloutput814,label=r'$\delta \rho (Dr)$14')
plt.plot(t_out,abs_log_fulloutput1014,label=r'$\delta \rho (DM1)$14')
plt.plot(t_out,abs_log_fulloutput1114,label=r'$\delta \rho (DM2)$14')


plt.title('Dark Perturbations - "Matter"')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####Perturbations - "fields"####


#abs_log_fulloutput96 = log10(((data6[:,3])**2.0)**0.5)
#abs_log_fulloutput86 = log10(((data6[:,2])**2.0)**0.5)
#abs_log_fulloutput106 = log10(((data6[:,10])**2.0)**0.5)
#abs_log_fulloutput116 = log10(((data6[:,11])**2.0)**0.5)

abs_log_fulloutput98 = log10(((data8[:,3])**2.0)**0.5)
abs_log_fulloutput88 = log10(((data8[:,2])**2.0)**0.5)
abs_log_fulloutput108 = log10(((data8[:,10])**2.0)**0.5)
abs_log_fulloutput118 = log10(((data8[:,11])**2.0)**0.5)

abs_log_fulloutput910 = log10(((data10[:,3])**2.0)**0.5)
abs_log_fulloutput810 = log10(((data10[:,2])**2.0)**0.5)
abs_log_fulloutput1010 = log10(((data10[:,10])**2.0)**0.5)
abs_log_fulloutput1110 = log10(((data10[:,11])**2.0)**0.5)

#abs_log_fulloutput912 = log10(((data12[:,3])**2.0)**0.5)
#abs_log_fulloutput812 = log10(((data12[:,2])**2.0)**0.5)

for x in range(0,tshape[0]):
    
    if x==0 : 
#        pDFresults6 = pDF0(data6[x,:],t_out[x])
#        pDF0Results6 = pDFresults6[0]
#        pDF20Results6 = pDFresults6[1]
        pDFresults8 = pDF0(data8[x,:],t_out[x])
        pDF0Results8 = pDFresults8[0]
        pDF20Results8 = pDFresults8[1]
        pDFresults10 = pDF0(data10[x,:],t_out[x])
        pDF0Results10 = pDFresults10[0]
        pDF20Results10 = pDFresults10[1]
        pDFresults12 = pDF0(data12[x,:],t_out[x])
        pDF0Results12 = pDFresults12[0]
        pDF20Results12 = pDFresults12[1]
        pDFresults14 = pDF0(data14[x,:],t_out[x])
        pDF0Results14 = pDFresults14[0]
        pDF20Results14 = pDFresults14[1]
    else:
#        pD6i = pDF0(data6[x,:],t_out[x])
#        pDF0Results6 = np.vstack([pDF0Results6,pD6i[0]])
#        pDF20Results6 = np.vstack([pDF20Results6,pD6i[1]])
        pD8i = pDF0(data8[x,:],t_out[x])
        pDF0Results8 = np.vstack([pDF0Results8,pD8i[0]])
        pDF20Results8 = np.vstack([pDF20Results8,pD8i[1]])
        pD10i = pDF0(data10[x,:],t_out[x])
        pDF0Results10 = np.vstack([pDF0Results10,pD10i[0]])
        pDF20Results10 = np.vstack([pDF20Results10,pD10i[1]])
        pD12i = pDF0(data12[x,:],t_out[x])
        pDF0Results12 = np.vstack([pDF0Results12,pD12i[0]])
        pDF20Results12 = np.vstack([pDF20Results12,pD12i[1]])
        pD14i = pDF0(data14[x,:],t_out[x])
        pDF0Results14 = np.vstack([pDF0Results14,pD14i[0]])
        pDF20Results14 = np.vstack([pDF20Results14,pD14i[1]])


#log_pDF0Results6=log10(((pDF0Results6)**2.0)**0.5)
log_pDF0Results8=log10(((pDF0Results8)**2.0)**0.5)
log_pDF0Results10=log10(((pDF0Results10)**2.0)**0.5)
log_pDF0Results12=log10(((pDF0Results12)**2.0)**0.5)
log_pDF0Results14=log10(((pDF0Results14)**2.0)**0.5)
#log_pDF20Results6=log10(((pDF20Results6)**2.0)**0.5)
log_pDF20Results8=log10(((pDF20Results8)**2.0)**0.5)
log_pDF20Results10=log10(((pDF20Results10)**2.0)**0.5)
log_pDF20Results12=log10(((pDF20Results12)**2.0)**0.5)
log_pDF20Results14=log10(((pDF20Results14)**2.0)**0.5)


#plt.plot(t_out,abs_log_fulloutput96,label=r'$\delta \rho (Db)$6')
#plt.plot(t_out,abs_log_fulloutput86,label=r'$\delta \rho (Dr)$6')
#plt.plot(t_out,abs_log_fulloutput106,label=r'$\delta \rho (DM1)$6')
#plt.plot(t_out,abs_log_fulloutput116,label=r'$\delta \rho (DM2)$6')
#plt.plot(t_out,log_pDF0Results6,label=r'$\delta \rho_{\phi_1}$6')
#plt.plot(t_out,log_pDF20Results6,label=r'$\delta \rho_{\phi_2}$6')

#plt.plot(t_out,abs_log_fulloutput98,label=r'$\delta \rho (Db)$8')
#plt.plot(t_out,abs_log_fulloutput88,label=r'$\delta \rho (Dr)$8')
#plt.plot(t_out,abs_log_fulloutput108,label=r'$\delta \rho (DM1)$8')
#plt.plot(t_out,abs_log_fulloutput118,label=r'$\delta \rho (DM2)$8')
plt.plot(t_out,log_pDF0Results8,label=r'$\delta \rho_{\phi_1}$8')
plt.plot(t_out,log_pDF20Results8,label=r'$\delta \rho_{\phi_2}$8')

#plt.plot(t_out,abs_log_fulloutput910,label=r'$\delta \rho (Db)$10')
#plt.plot(t_out,abs_log_fulloutput810,label=r'$\delta \rho (Dr)$10')
#plt.plot(t_out,abs_log_fulloutput1010,label=r'$\delta \rho (DM1)$10')
#plt.plot(t_out,abs_log_fulloutput1110,label=r'$\delta \rho (DM2)$10')
plt.plot(t_out,log_pDF0Results10,label=r'$\delta \rho_{\phi_1}$10')
plt.plot(t_out,log_pDF20Results10,label=r'$\delta \rho_{\phi_2}$10')

#plt.plot(t_out,abs_log_fulloutput912,label=r'$\delta \rho (Db)$12')
#plt.plot(t_out,abs_log_fulloutput812,label=r'$\delta \rho (Dr)$12')
plt.plot(t_out,log_pDF0Results12,label=r'$\delta \rho_{\phi_1}$12')
plt.plot(t_out,log_pDF20Results12,label=r'$\delta \rho_{\phi_2}$12')

plt.plot(t_out,log_pDF0Results14,label=r'$\delta \rho_{\phi_1}$14')
plt.plot(t_out,log_pDF20Results14,label=r'$\delta \rho_{\phi_2}$14')


plt.title('Dark Perturbations - "fields"')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()



####Density Contrast####

PHIi=(kvko*H_o/keq)**(-3.0/2.0)

#dcon96 = (data6[:,3]/data6[:,1])
#dcon86 = (data6[:,2]/data6[:,0])
#kkodcon96=-((PHIi)**(-1.0))*dcon96
#kkodcon86=-((PHIi)**(-1.0))*dcon86
#plt.plot(t_out,kkodcon96,label=r'$\delta (Db)$6')
#plt.plot(t_out,kkodcon86,label=r'$\delta (Dr)$6')

dcon98 = (data8[:,3]/data8[:,1])
dcon88 = (data8[:,2]/data8[:,0])
kkodcon98=-((PHIi)**(-1.0))*dcon98
kkodcon88=-((PHIi)**(-1.0))*dcon88
plt.plot(t_out,kkodcon98,label=r'$\delta (Db)$8')
plt.plot(t_out,kkodcon88,label=r'$\delta (Dr)$8')

dcon910 = (data10[:,3]/data10[:,1])
dcon810 = (data10[:,2]/data10[:,0])
kkodcon910=-((PHIi)**(-1.0))*dcon910
kkodcon810=-((PHIi)**(-1.0))*dcon810
plt.plot(t_out,kkodcon910,label=r'$\delta (Db)$10')
plt.plot(t_out,kkodcon810,label=r'$\delta (Dr)$10')

dcon912 = (data12[:,3]/data12[:,1])
dcon812 = (data12[:,2]/data12[:,0])
kkodcon912=-((PHIi)**(-1.0))*dcon912
kkodcon812=-((PHIi)**(-1.0))*dcon812
plt.plot(t_out,kkodcon912,label=r'$\delta (Db)$12')
plt.plot(t_out,kkodcon812,label=r'$\delta (Dr)$12')

dcon914 = (data14[:,3]/data14[:,1])
dcon814 = (data14[:,2]/data14[:,0])
kkodcon914=-((PHIi)**(-1.0))*dcon914
kkodcon814=-((PHIi)**(-1.0))*dcon814
plt.plot(t_out,kkodcon914,label=r'$\delta (Db)$14')
plt.plot(t_out,kkodcon814,label=r'$\delta (Dr)$14')

plt.title('-((PHIi)**(-1.0)) Dens cont mad and rad')
plt.xlabel('e-folds')
plt.ylabel(' dens cont')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####sigma####
#dcon96 = (data6[:,3]/data6[:,1])
#dcon86 = (data6[:,2]/data6[:,0])
#kkodcon96=-((PHIi)**(-1.0))*dcon96
#kkodcon86=-((PHIi)**(-1.0))*dcon86

dcon98 = (data8[:,3]/data8[:,1])
dcon88 = (data8[:,2]/data8[:,0])
kkodcon98=-((PHIi)**(-1.0))*dcon98
kkodcon88=-((PHIi)**(-1.0))*dcon88

dcon910 = (data10[:,3]/data10[:,1])
dcon810 = (data10[:,2]/data10[:,0])
kkodcon910=-((PHIi)**(-1.0))*dcon910
kkodcon810=-((PHIi)**(-1.0))*dcon810

dcon912 = (data12[:,3]/data12[:,1])
dcon812 = (data12[:,2]/data12[:,0])
kkodcon912=-((PHIi)**(-1.0))*dcon912
kkodcon812=-((PHIi)**(-1.0))*dcon812

dcon914 = (data14[:,3]/data14[:,1])
dcon814 = (data14[:,2]/data14[:,0])
kkodcon914=-((PHIi)**(-1.0))*dcon914
kkodcon814=-((PHIi)**(-1.0))*dcon814

#sigma6=(3.0/4.0)*dcon86 - dcon96
sigma8=(3.0/4.0)*dcon88 - dcon98
sigma10=(3.0/4.0)*dcon810 - dcon910
sigma12=(3.0/4.0)*dcon812 - dcon912
sigma14=(3.0/4.0)*dcon814 - dcon914


#plt.plot(t_out,log10(1.0+sigma6),label=r'$\sigma$6')
plt.plot(t_out,log10(1.0+sigma8),label=r'$\sigma$8')
plt.plot(t_out,log10(1.0+sigma10),label=r'$\sigma$10')
plt.plot(t_out,log10(1.0+sigma12),label=r'$\sigma$12')
plt.plot(t_out,log10(1.0+sigma14),label=r'$\sigma$14')


plt.title('sigma')
plt.xlabel('e-folds')
plt.ylabel('log_10(1+sigma)')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()


####ZETA####


for x in range(0,tshape[0]):
    
    if x==0 : 
#        ZETARESULTS6 = ZETA(data6[x,:],t_out[x])
        ZETARESULTS8 = ZETA(data8[x,:],t_out[x])
        ZETARESULTS10 = ZETA(data10[x,:],t_out[x])
        ZETARESULTS12 = ZETA(data12[x,:],t_out[x])
        ZETARESULTS14 = ZETA(data14[x,:],t_out[x])
    else:
#        Z6i = ZETA(data6[x,:],t_out[x])
#        ZETARESULTS6 = np.vstack([ZETARESULTS6,Z6i])
        Z8i = ZETA(data8[x,:],t_out[x])
        ZETARESULTS8 = np.vstack([ZETARESULTS8,Z8i])
        Z10i = ZETA(data10[x,:],t_out[x])
        ZETARESULTS10 = np.vstack([ZETARESULTS10,Z10i])
        Z12i = ZETA(data12[x,:],t_out[x])        
        ZETARESULTS12 = np.vstack([ZETARESULTS12,Z12i])
        Z14i = ZETA(data14[x,:],t_out[x])        
        ZETARESULTS14 = np.vstack([ZETARESULTS14,Z14i])


#log_ZETAResults6 = log10(((ZETARESULTS6)**2.0)**0.5)
log_ZETAResults8 = log10(((ZETARESULTS8)**2.0)**0.5)
log_ZETAResults10 = log10(((ZETARESULTS10)**2.0)**0.5)
log_ZETAResults12 = log10(((ZETARESULTS12)**2.0)**0.5)
log_ZETAResults14 = log10(((ZETARESULTS14)**2.0)**0.5)

#plt.plot(t_out,log_ZETAResults6,label=r'$\zeta$6')
plt.plot(t_out,log_ZETAResults8,label=r'$\zeta$8')
plt.plot(t_out,log_ZETAResults10,label=r'$\zeta$10')
plt.plot(t_out,log_ZETAResults12,label=r'$\zeta$12')
plt.plot(t_out,log_ZETAResults14,label=r'$\zeta$14')



plt.title('Zeta')
plt.xlabel('e-folds')
plt.ylabel('log Zeta')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()


#################################################
###All remaining values##########################
#################################################

###Phi###

for x in range(0,tshape[0]):
    
    if x==0 : 
#        PhiResults6 = X(data6[x,:],t_out[x])
        PhiResults8 = X(data8[x,:],t_out[x])
        PhiResults10 = X(data10[x,:],t_out[x])
        PhiResults12 = X(data12[x,:],t_out[x])
        PhiResults14 = X(data14[x,:],t_out[x])
    else:
#        Z6i = X(data6[x,:],t_out[x])
#        PhiResults6 = np.vstack([PhiResults6,Z6i])
        Z8i = X(data8[x,:],t_out[x])
        PhiResults8 = np.vstack([PhiResults8,Z8i])
        Z10i = X(data10[x,:],t_out[x])
        PhiResults10 = np.vstack([PhiResults10,Z10i])
        Z12i = X(data12[x,:],t_out[x])        
        PhiResults12 = np.vstack([PhiResults12,Z12i])
        Z14i = X(data14[x,:],t_out[x])        
        PhiResults14 = np.vstack([PhiResults14,Z14i])


#log_PhiResults6 = log10(((PhiResults6)**2.0)**0.5)
log_PhiResults8 = log10(((PhiResults8)**2.0)**0.5)
log_PhiResults10 = log10(((PhiResults10)**2.0)**0.5)
log_PhiResults12 = log10(((PhiResults12)**2.0)**0.5)
log_PhiResults14 = log10(((PhiResults14)**2.0)**0.5)

#kkoPhiResults6 = ((PHIi)**(-1.0))*(((PhiResults6)**2.0)**0.5)
kkoPhiResults8 = ((PHIi)**(-1.0))*(((PhiResults8)**2.0)**0.5)
kkoPhiResults10 = ((PHIi)**(-1.0))*(((PhiResults10)**2.0)**0.5)
kkoPhiResults12 = ((PHIi)**(-1.0))*(((PhiResults12)**2.0)**0.5)
kkoPhiResults14 = ((PHIi)**(-1.0))*(((PhiResults14)**2.0)**0.5)

#
#plt.plot(t_out,kkoPhiResults6,label=r'((PHIi)**(-1.0)) $\Phi$6')
plt.plot(t_out,kkoPhiResults8,label=r'((PHIi)**(-1.0)) $\Phi$8')
plt.plot(t_out,kkoPhiResults10,label=r'((PHIi)**(-1.0)) $\Phi$10')
plt.plot(t_out,kkoPhiResults12,label=r'((PHIi)**(-1.0)) $\Phi$12')
plt.plot(t_out,kkoPhiResults14,label=r'((PHIi)**(-1.0)) $\Phi$14')

plt.title('$\Phi$')
plt.xlabel('e-folds')
plt.ylabel('((PHIi)**(-1.0)) $\Phi$')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()


####Field Perturbation####

#abs_log_fulloutput166 = log10(((data6[:,16])**2.0)**0.5)
#abs_log_fulloutput176 = log10(((data6[:,17])**2.0)**0.5)
#plt.plot(t_out,abs_log_fulloutput166,label=r'log $\delta \phi_1$6')
#plt.plot(t_out,abs_log_fulloutput176,label=r'log $\delta \phi_2$6')

abs_log_fulloutput168 = log10(((data8[:,16])**2.0)**0.5)
abs_log_fulloutput178 = log10(((data8[:,17])**2.0)**0.5)
plt.plot(t_out,abs_log_fulloutput168,label=r'log $\delta \phi_1$8')
plt.plot(t_out,abs_log_fulloutput178,label=r'log $\delta \phi_2$8')

abs_log_fulloutput1610 = log10(((data10[:,16])**2.0)**0.5)
abs_log_fulloutput1710 = log10(((data10[:,17])**2.0)**0.5)
plt.plot(t_out,abs_log_fulloutput1610,label=r'log $\delta \phi_1$10')
plt.plot(t_out,abs_log_fulloutput1710,label=r'log $\delta \phi_2$10')

abs_log_fulloutput1612 = log10(((data12[:,16])**2.0)**0.5)
abs_log_fulloutput1712 = log10(((data12[:,17])**2.0)**0.5)
plt.plot(t_out,abs_log_fulloutput1612,label=r'log $\delta \phi_1$1012')
plt.plot(t_out,abs_log_fulloutput1712,label=r'log $\delta \phi_2$1012')

abs_log_fulloutput1614 = log10(((data14[:,16])**2.0)**0.5)
abs_log_fulloutput1714 = log10(((data14[:,17])**2.0)**0.5)
plt.plot(t_out,abs_log_fulloutput1614,label=r'log $\delta \phi_1$1014')
plt.plot(t_out,abs_log_fulloutput1714,label=r'log $\delta \phi_2$1014')



plt.title('Field perturbations')
plt.xlabel('e-folds')
plt.ylabel('log Field perturbations values')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

#print data6
print data8
print data10
print data12
print data14

##############Log plots section! -end- #################
