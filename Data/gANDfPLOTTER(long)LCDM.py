# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# E X A M P L E   1   -   A   S I N G L E   R U N   W I T H               #
# O N E   S E T   O F   C O U P L I N G S   T O                           #
# D E M O N S T R A T I N G   P L O T T I N G   O F   P A R A M E T E R S #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

###########################################################################
###########################################################################
#R E M E M B E R  T O  S E T  ks  I N  E A C H  F U N C T I O N ! ! ! ! ! #
###########################################################################
###########################################################################

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin

from CONSTANTS import *
from MODELLCDM import *
from BACKGROUND import *
from PERTURBED import *

data = np.load('LCDMOUTPUTk2H0.npy')
data2 = np.load('LCDMOUTPUTk10H0.npy')
data3 = np.load('LCDMOUTPUTk50H0.npy')
data4 = np.load('LCDMOUTPUTk100H0.npy')
data5 = np.load('LCDMOUTPUTk200H0.npy')
data6 = np.load('LCDMOUTPUTk300H0.npy')
data7 = np.load('LCDMOUTPUTk400H0.npy')
data8 = np.load('LCDMOUTPUTk500H0.npy')
data9 = np.load('LCDMOUTPUTk600H0.npy')
data10 = np.load('LCDMOUTPUTk700H0.npy')
data11 = np.load('LCDMOUTPUTk800H0.npy')
data12 = np.load('LCDMOUTPUTk900H0.npy')
data13 = np.load('LCDMOUTPUTk1000H0.npy')


t_out = np.load('LCDMTIMEk2H0.npy')


####################Times############################
t_i=log(10.0**(-6.0));t_f=log(10.0**(0.5));step=0.01
#####################################################


tshape = t_out.shape
print tshape
now=-int(t_i/step) + 1 
print now

datanow=data[now,:]
data2now=data2[now,:]
data3now=data3[now,:]
data4now=data4[now,:]
data5now=data5[now,:]
data6now=data6[now,:]
data7now=data7[now,:]
data8now=data8[now,:]
data9now=data9[now,:]
data10now=data10[now,:]
data11now=data11[now,:]
data12now=data12[now,:]
data13now=data13[now,:]

tnow=t_out[now]
anow=exp(tnow)
Bnow=Y(datanow,tnow)
B2now=Y(data2now,tnow)
B3now=Y(data3now,tnow)
B4now=Y(data4now,tnow)
B5now=Y(data5now,tnow)
B6now=Y(data6now,tnow)
B7now=Y(data7now,tnow)
B8now=Y(data8now,tnow)
B9now=Y(data9now,tnow)
B10now=Y(data10now,tnow)
B11now=Y(data11now,tnow)
B12now=Y(data12now,tnow)
B13now=Y(data13now,tnow)


delta0=(np.sum((((datanow[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + datanow[2:2+(A-1)+1]*anow*Bnow)**2.0)**0.5)/(datanow[2:2+(A-1)+1])))
delta02=(np.sum((((data2now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data2now[2:2+(A-1)+1]*anow*B2now)**2.0)**0.5)/(data2now[2:2+(A-1)+1])))
delta03=(np.sum((((data3now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data3now[2:2+(A-1)+1]*anow*B3now)**2.0)**0.5)/(data3now[2:2+(A-1)+1])))
delta04=(np.sum((((data4now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data4now[2:2+(A-1)+1]*anow*B4now)**2.0)**0.5)/(data4now[2:2+(A-1)+1])))
delta05=(np.sum((((data5now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data5now[2:2+(A-1)+1]*anow*B5now)**2.0)**0.5)/(data5now[2:2+(A-1)+1])))
delta06=(np.sum((((data6now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data6now[2:2+(A-1)+1]*anow*B6now)**2.0)**0.5)/(data6now[2:2+(A-1)+1])))
delta07=(np.sum((((data7now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data7now[2:2+(A-1)+1]*anow*B7now)**2.0)**0.5)/(data7now[2:2+(A-1)+1])))
delta08=(np.sum((((data8now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data8now[2:2+(A-1)+1]*anow*B8now)**2.0)**0.5)/(data8now[2:2+(A-1)+1])))
delta09=(np.sum((((data9now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data9now[2:2+(A-1)+1]*anow*B9now)**2.0)**0.5)/(data9now[2:2+(A-1)+1])))
delta010=(np.sum((((data10now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data10now[2:2+(A-1)+1]*anow*B10now)**2.0)**0.5)/(data10now[2:2+(A-1)+1])))
delta011=(np.sum((((data11now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data11now[2:2+(A-1)+1]*anow*B11now)**2.0)**0.5)/(data11now[2:2+(A-1)+1])))
delta012=(np.sum((((data12now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data12now[2:2+(A-1)+1]*anow*B12now)**2.0)**0.5)/(data12now[2:2+(A-1)+1])))
delta013=(np.sum((((data13now[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + data13now[2:2+(A-1)+1]*anow*B13now)**2.0)**0.5)/(data13now[2:2+(A-1)+1])))

def growth(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 2.0*H_o
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta0))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth2(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 10.0*H_o
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta02))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth3(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 50.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta03))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth4(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 100.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta04))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth5(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 200.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta05))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth6(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 300.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta06))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth7(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 400.0*H_o
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta07))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth8(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 500.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta08))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth9(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 600.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta09))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth10(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 700.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta010))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth11(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)
    k= 800.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta011))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth12(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 900.0*H_o
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta012))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))

def growth13(In,t) :
    Hin=H(In,t)
    dDMin=dDM0(In,t)
    dpDM0in=dpDM0(In,t)    
    k= 1000.0*H_o   
    Bin=Y(In,t)
    ain=exp(t)
    return (1.0/(delta013))*((np.sum((((In[7+(A-1)+2*(I-1):7+2*(A-1)+2*(I-1)+1] + In[2:2+(A-1)+1]*ain*Bin)**2.0)**0.5)/(In[2:2+(A-1)+1]))))


for x in range(0,tshape[0]):
    
    if x==0 : 
        GrowthResults = growth(data[x,:],t_out[x])
        Growth2Results = growth2(data2[x,:],t_out[x])
        Growth3Results = growth3(data3[x,:],t_out[x])
        Growth4Results = growth4(data4[x,:],t_out[x])
        Growth5Results = growth5(data5[x,:],t_out[x])
        Growth6Results = growth6(data6[x,:],t_out[x])
        Growth7Results = growth7(data7[x,:],t_out[x])
        Growth8Results = growth8(data8[x,:],t_out[x])
        Growth9Results = growth9(data9[x,:],t_out[x])
        Growth10Results = growth10(data10[x,:],t_out[x])
        Growth11Results = growth11(data11[x,:],t_out[x])
        Growth12Results = growth12(data12[x,:],t_out[x])
        Growth13Results = growth13(data13[x,:],t_out[x])
    else:
        Gi=growth(data[x,:],t_out[x])
        G2i=growth2(data2[x,:],t_out[x])
        G3i=growth3(data3[x,:],t_out[x])
        G4i=growth4(data4[x,:],t_out[x])
        G5i=growth5(data5[x,:],t_out[x])
        G6i = growth6(data6[x,:],t_out[x])
        G7i = growth7(data7[x,:],t_out[x])
        G8i = growth8(data8[x,:],t_out[x])
        G9i = growth9(data9[x,:],t_out[x])
        G10i = growth10(data10[x,:],t_out[x])
        G11i = growth11(data11[x,:],t_out[x])
        G12i = growth12(data12[x,:],t_out[x])
        G13i = growth13(data13[x,:],t_out[x])
        GrowthResults = np.vstack([GrowthResults,Gi])
        Growth2Results = np.vstack([Growth2Results,G2i])
        Growth3Results = np.vstack([Growth3Results,G3i])
        Growth4Results = np.vstack([Growth4Results,G4i])
        Growth5Results = np.vstack([Growth5Results,G5i])
        Growth6Results = np.vstack([Growth6Results,G6i])
        Growth7Results = np.vstack([Growth7Results,G7i])
        Growth8Results = np.vstack([Growth8Results,G8i])
        Growth9Results = np.vstack([Growth9Results,G9i])
        Growth10Results = np.vstack([Growth10Results,G10i])
        Growth11Results = np.vstack([Growth11Results,G11i])
        Growth12Results = np.vstack([Growth12Results,G12i])
        Growth13Results = np.vstack([Growth13Results,G13i])

####Growth Factor####

#
plt.plot(t_out,GrowthResults,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=2H_o')
plt.plot(t_out,Growth2Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=10H_o')
plt.plot(t_out,Growth3Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=50H_o')
plt.plot(t_out,Growth4Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=100H_o')
plt.plot(t_out,Growth5Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=200H_o')
plt.plot(t_out,Growth6Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=300H_o')
plt.plot(t_out,Growth7Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=400H_o')
plt.plot(t_out,Growth8Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=500H_o',color='0.40')
plt.plot(t_out,Growth9Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=600H_o',color='0.50')
plt.plot(t_out,Growth10Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=700H_o',color='0.60')
plt.plot(t_out,Growth11Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=800H_o',color='0.70')
plt.plot(t_out,Growth12Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=900H_o',color='0.80')
plt.plot(t_out,Growth13Results,label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=1000H_o',color='0.90')

plt.title(r'Growth, g, $\frac{\delta}{\delta_0}$ for LCDM - long')
plt.xlabel('e-folds')
plt.ylabel('g')
#
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

####Log Growth Factor####

#
plt.plot(t_out,log10(GrowthResults),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=2H_o')
plt.plot(t_out,log10(Growth2Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=10H_o')
plt.plot(t_out,log10(Growth3Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=50H_o')
plt.plot(t_out,log10(Growth4Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=100H_o')
plt.plot(t_out,log10(Growth5Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=200H_o')
plt.plot(t_out,log10(Growth6Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=300H_o')
plt.plot(t_out,log10(Growth7Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=400H_o')
plt.plot(t_out,log10(Growth8Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=500H_o',color='0.40')
plt.plot(t_out,log10(Growth9Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=600H_o',color='0.50')
plt.plot(t_out,log10(Growth10Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=700H_o',color='0.60')
plt.plot(t_out,log10(Growth11Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=800H_o',color='0.70')
plt.plot(t_out,log10(Growth12Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=900H_o',color='0.80')
plt.plot(t_out,log10(Growth13Results),label=r'Growth, g, $\frac{\delta}{\delta_0}$, k=1000H_o',color='0.90')

plt.title(r'Log Growth, g, $\frac{\delta}{\delta_0}$ for LCDM - long')
plt.xlabel('e-folds')
plt.ylabel('g')
#

##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

delta=GrowthResults*delta0
delta2=Growth2Results*delta02
delta3=Growth3Results*delta03
delta4=Growth4Results*delta04
delta5=Growth5Results*delta05
delta6=Growth6Results*delta06
delta7=Growth7Results*delta07
delta8=Growth8Results*delta08
delta9=Growth9Results*delta09
delta10=Growth10Results*delta010
delta11=Growth11Results*delta011
delta12=Growth12Results*delta012
delta13=Growth13Results*delta013

for x in range(0,tshape[0]):
    
    if x==0 : 
        fResults=(delta[x+1]-delta[x])/(step*delta[x])
        f2Results=(delta2[x+1]-delta2[x])/(step*delta2[x])
        f3Results=(delta3[x+1]-delta3[x])/(step*delta3[x])
        f4Results=(delta4[x+1]-delta4[x])/(step*delta4[x])
        f5Results=(delta5[x+1]-delta5[x])/(step*delta5[x])
        f6Results=(delta6[x+1]-delta6[x])/(step*delta6[x])
        f7Results=(delta7[x+1]-delta7[x])/(step*delta7[x])
        f8Results=(delta8[x+1]-delta8[x])/(step*delta8[x])
        f9Results=(delta9[x+1]-delta9[x])/(step*delta9[x])
        f10Results=(delta10[x+1]-delta10[x])/(step*delta10[x])
        f11Results=(delta11[x+1]-delta11[x])/(step*delta11[x])
        f12Results=(delta12[x+1]-delta12[x])/(step*delta12[x])
        f13Results=(delta13[x+1]-delta13[x])/(step*delta13[x])
    else:
        fR=(delta[x]-delta[x-1])/(step*delta[x])
        f2R=(delta2[x]-delta2[x-1])/(step*delta2[x])
        f3R=(delta3[x]-delta3[x-1])/(step*delta3[x])
        f4R=(delta4[x]-delta4[x-1])/(step*delta4[x])
        f5R=(delta5[x]-delta5[x-1])/(step*delta5[x])
        f6R=(delta6[x]-delta6[x-1])/(step*delta6[x])
        f7R=(delta7[x]-delta7[x-1])/(step*delta7[x])
        f8R=(delta8[x]-delta8[x-1])/(step*delta8[x])
        f9R=(delta9[x]-delta9[x-1])/(step*delta9[x])
        f10R=(delta10[x]-delta10[x-1])/(step*delta10[x])
        f11R=(delta11[x]-delta11[x-1])/(step*delta11[x])
        f12R=(delta12[x]-delta12[x-1])/(step*delta12[x])
        f13R=(delta13[x]-delta13[x-1])/(step*delta13[x])
        fResults=np.vstack([fResults,fR])
        f2Results=np.vstack([f2Results,f2R])
        f3Results=np.vstack([f3Results,f3R])
        f4Results=np.vstack([f4Results,f4R])
        f5Results=np.vstack([f5Results,f5R])
        f6Results=np.vstack([f6Results,f6R])
        f7Results=np.vstack([f7Results,f7R])
        f8Results=np.vstack([f8Results,f8R])
        f9Results=np.vstack([f9Results,f9R])
        f10Results=np.vstack([f10Results,f10R])
        f11Results=np.vstack([f11Results,f11R])
        f12Results=np.vstack([f12Results,f12R])
        f13Results=np.vstack([f13Results,f13R])

####Growth Factor####

#
plt.plot(t_out,fResults,label=r'$f$, k=2H_o')
plt.plot(t_out,f2Results,label=r'$f$, k=10H_o')
plt.plot(t_out,f3Results,label=r'$f$, k=50H_o')
plt.plot(t_out,f4Results,label=r'$f$, k=100H_o')
plt.plot(t_out,f5Results,label=r'$f$, k=200H_o')
plt.plot(t_out,f6Results,label=r'$f$, k=300H_o')
plt.plot(t_out,f7Results,label=r'$f$, k=400H_o')
plt.plot(t_out,f8Results,label=r'$f$, k=500H_o',color='0.40')
plt.plot(t_out,f9Results,label=r'$f$, k=600H_o',color='0.50')
plt.plot(t_out,f10Results,label=r'$f$, k=700H_o',color='0.60')
plt.plot(t_out,f11Results,label=r'$f$, k=800H_o',color='0.70')
plt.plot(t_out,f12Results,label=r'$f$, k=900H_o',color='0.80')
plt.plot(t_out,f13Results,label=r'$f$, k=1000H_o',color='0.90')


plt.title('f for LCDM - long')
plt.xlabel('e-folds')
plt.ylabel('f')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()

####Log Growth Factor####

#
plt.plot(t_out,log10(fResults),label=r'$f$, k=2H_o')
plt.plot(t_out,log10(f2Results),label=r'$f$, k=10H_o')
plt.plot(t_out,log10(f3Results),label=r'$f$, k=50H_o')
plt.plot(t_out,log10(f4Results),label=r'$f$, k=100H_o')
plt.plot(t_out,log10(f5Results),label=r'$f$, k=200H_o')
plt.plot(t_out,log10(f6Results),label=r'$f$, k=300H_o')
plt.plot(t_out,log10(f7Results),label=r'$f$, k=400H_o')
plt.plot(t_out,log10(f8Results),label=r'$f$, k=500H_o',color='0.40')
plt.plot(t_out,log10(f9Results),label=r'$f$, k=600H_o',color='0.50')
plt.plot(t_out,log10(f10Results),label=r'$f$, k=700H_o',color='0.60')
plt.plot(t_out,log10(f11Results),label=r'$f$, k=800H_o',color='0.70')
plt.plot(t_out,log10(f12Results),label=r'$f$, k=900H_o',color='0.80')
plt.plot(t_out,log10(f13Results),label=r'$f$, k=1000H_o',color='0.90')


plt.title('Log f for LCDM - long')
plt.xlabel('e-folds')
plt.ylabel('Log f')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()



