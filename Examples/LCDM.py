# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# E X A M P L E   1   -   A   S I N G L E   R U N   W I T H               #
# O N E   S E T   O F   C O U P L I N G S   T O                           #
# D E M O N S T R A T I N G   P L O T T I N G   O F   P A R A M E T E R S #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy ODE solver 

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin


from CONSTANTS import *
from MODELLCDM import *
from BACKGROUND import *
from PERTURBED import *

####################Times############################
t_i=log(10.0**(-6.0));t_f=log(10.0**(0.5));step=0.01
#####################################################

##########ICs necessary for H and Potentials ########

f_0[3]=x1_o#*(1.0+(10.0**(-8.0)))#x1_o - ((K)/(C[0,0]-C[0,1]))*(log((-C[1,1]*ODM20_o)/(C[1,0]*ODM10_o)))

f_0[0]=Or_o*Dc_o*(exp(-4.0*t_i));f_0[1]=Ob_o*Dc_o*(exp(-3.0*t_i))
f_0[2]=ODM10_o*Dc_o*exp(-3.0*t_i)

f_0[4]=0.0;f_0[5]=0.0;f_0[6]=0.0;f_0[7]=0.0;f_0[8]=0.0;f_0[9]=0.0;f_0[10]=0.0
f_0[11]=(10.0**(-5.0))*f_0[3]*0.0;f_0[12]=0.0


ai=exp(t_i)
Hi= H(f_0,t_i)
VIi=VI(f_0,t_i)
V1i= VIi[0]
VPi= VP(f_0,t_i)
V1Pi = VPi[0]
PHIi=(k/keq)**(-3.0/2.0)
Bi=-PHIi/(Hi*ai)
dDr0i=dDr0(f_0,t_i)
dDb0i=dDb0(f_0,t_i)

print (k**2.0)/(ai**2.0)
print (K**2.0)/(2.0*Hi)
print (K**2.0)
print (Hi**2.0)
print VIi


#######Remaining ICs dependent upon H and Potentials############

f_0[5]=-(2.0)*PHIi*f_0[0]+ 4.0*Hi*ai*f_0[0]*Bi;f_0[6]=-(3.0/2.0)*PHIi*f_0[1]+ 3.0*Hi*ai*f_0[1]*Bi;f_0[7]=-(3.0/2.0)*PHIi*f_0[2]+ 3.0*Hi*ai*f_0[2]*Bi
f_0[8]=((3.0*ai*Hi*(f_0[2] + f_0[1] + (4.0/3.0)*f_0[0]))**(-1.0))*(f_0[7] - 3.0*Hi*ai*f_0[2]*Bi + f_0[6] - 3.0*Hi*ai*f_0[1]*Bi + f_0[5] - 4.0*Hi*ai*f_0[0]*Bi + f_0[12]*f_0[4] + V1Pi*f_0[11] -3.0*Hi*(f_0[4]*f_0[11])  - (f_0[4]**2.0)*PHIi + (2.0*(k**2.0)*PHIi)/((K*ai)**2.0)) + Bi
f_0[9]=f_0[8];f_0[10]=f_0[8]

#########################
#####THE INTEGRATOR!#####
#########################

#Set up the time output required by ODE

t_out = np.arange(t_i,t_f,step) # start, end, interval

#Now, actually do the numerical integration

print PHIi


###########################
####NEW ODE SOLVER#########
###########################

#set up the integrator
r = scipy.integrate.ode(df,jac=None).set_integrator('dopri5',rtol=1E-14,atol=1E-14,nsteps=1E8)#works, starts slower speeds steadily
#r = scipy.integrate.ode(df,jac=None).set_integrator('vode',method='bdf',rtol=1E-8,atol=1E-8,nsteps=1E8)#starts -very- slow, speeds up to high speed toward end
#r = scipy.integrate.ode(df,jac=None).set_integrator('lsoda', rtol=1E-12,atol=1E-12,nsteps=1E8)
#r = scipy.integrate.ode(df,jac=None).set_integrator('zvode', method='bdf')
#r = scipy.integrate.ode(df,jac=None).set_integrator('vode',method='bdf')
#r = scipy.integrate.ode(df,jac=None).set_integrator('dop853',rtol=1E-10,atol=1E-10,nsteps=1E8)#works, starts slower speeds steadily


r.set_initial_value(f_0,t_i) #feed it the initial values

#run the integrator - results to list
while r.successful() and r.t < t_f :
      print r.t
      if r.t==t_i : 
         fulloutput=np.array(r.y)
         DFresults = DF0(fulloutput,r.t)
         DF0Results = DFresults[0]
         pDFresults = pDF0(fulloutput,r.t)
         pDF0Results = pDFresults[0]
         ZETAResults = ZETA(fulloutput,r.t)         
         PhiResults = X(fulloutput,r.t)         
      else:
         res=np.array(r.y)
         fulloutput = np.vstack([fulloutput, res])
         DF = DF0(res,r.t)
         DFi= DF[0]
         DF0Results = np.vstack([DF0Results,DFi])
         pDF = pDF0(res,r.t)
         pDFi= pDF[0]
         pDF0Results = np.vstack([pDF0Results,pDFi])
         ZETAi= ZETA(res,r.t)
         ZETAResults = np.vstack([ZETAResults,ZETAi])
         Xi= X(res,r.t)
         PhiResults = np.vstack([PhiResults,Xi])
      r.integrate(r.t+step)

##########################
####NEW ODE SOLVER END####
##########################

#######################################
#############save data#################
#######################################

out_name = "../Data/LCDMOUTPUTK3"

np.save(out_name,fulloutput)

out_name2 = "../Data/LCDMTIMEK3"

np.save(out_name2,t_out)

#######################################
#######################################
#######################################

##############Log plots section! -start- #################

####Background####

log_fulloutput = log10(((fulloutput)**2.0)**0.5)
log_DF0Results = log10(DF0Results)
log_DF0and20Results = log10(DF0Results+0.0)
log_bandrResults = log10(fulloutput[:,1]+fulloutput[:,0])

plt.plot(t_out,log_fulloutput[:,2],label=r'$\rho_{DM1}$')
#plt.plot(t_out,log_fulloutput[:,3],label=r'$x1$')
#plt.plot(t_out,log_fulloutput[:,4],label=r'$x2$')
plt.plot(t_out,log_DF0and20Results,label=r'$\Lambda$')
#plt.plot(t_out,log_DF20Results,label=r'$\rho_{\phi_2}$')
plt.plot(t_out,log_bandrResults,label=r'$(\rho_b + \rho_r )$')
plt.title('Background Densities')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
#p.ylim([-55,-25])
#p.xlim([-12,-11])

#plt.legend()
plt.show()

####Perturbations####

log_pDF0Results = log10(((pDF0Results)**2.0)**0.5)
abs_log_fulloutput10 = log10(((fulloutput[:,7])**2.0)**0.5)
#abs_log_fulloutput11 = log10(((fulloutput[:,11])**2.0)**0.5)
abs_log_fulloutput9 = log10(((fulloutput[:,6])**2.0)**0.5)
abs_log_fulloutput8 = log10(((fulloutput[:,5])**2.0)**0.5)

#
plt.plot(t_out,abs_log_fulloutput10,label=r'$\delta \rho_{DM1}$')
#plt.plot(t_out,abs_log_fulloutput11,label=r'$\delta \rho_{DM2}$')
plt.plot(t_out,abs_log_fulloutput9,label=r'$\delta \rho_{Db}$')
plt.plot(t_out,abs_log_fulloutput8,label=r'$\delta \rho_{Dr}$')
#plt.plot(t_out,log_pDF0Results,label=r'$\delta \rho_{\phi_1}$')
#plt.plot(t_out,log_pDF20Results,label=r'$\delta \rho_{\phi_2}$')
plt.title('Perturbations')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
#plt.legend()
plt.show()

####Density Contrast####
dcon9 = (fulloutput[:,6]/fulloutput[:,1])
dcon8 = (fulloutput[:,5]/fulloutput[:,0])
kkodcon9=-((PHIi)**(-1.0))*dcon9
kkodcon8=-((PHIi)**(-1.0))*dcon8
dcon10 = (fulloutput[:,7]/fulloutput[:,2])
kkodcon10=-((PHIi)**(-1.0))*dcon10

plt.plot(t_out,kkodcon9,label=r'$\delta_{Db}$')
plt.plot(t_out,kkodcon8,label=r'$\delta_{Dr}$')
plt.plot(t_out,kkodcon10,label=r'$\delta_{DM}$')
plt.title('-((k/k_eq)^(3/2)) Density Contrasts')
plt.xlabel('e-folds')
plt.ylabel(' dens cont')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
#plt.legend()
plt.show()

#####sigma####
#dcon9 = (fulloutput[:,9]/fulloutput[:,1])
#dcon8 = (fulloutput[:,8]/fulloutput[:,0])
#kkodcon9=-((PHIi)**(-1.0))*dcon9
#kkodcon8=-((PHIi)**(-1.0))*dcon8
#sigma=(3.0/4.0)*dcon8 - dcon9
#plt.plot(t_out,log10(1.0+sigma),label=r'$\sigma$')
#plt.title('sigma')
#plt.xlabel('e-folds')
#plt.ylabel('log_10(1+sigma)')
##
#box = plt.subplot(111).get_position()
#plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
#plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
###p.ylim([-55,-25])
###p.xlim([-12,-11])
##
#plt.legend()
#plt.show()
#
#
#####ZETA####
#
#log_ZETAResults = log10(((ZETAResults)**2.0)**0.5)
#
#plt.plot(t_out,log_ZETAResults,label=r'$\zeta$')
#
#plt.title('Zeta')
#plt.xlabel('e-folds')
#plt.ylabel('log Zeta')
#
#box = plt.subplot(111).get_position()
#plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
#plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
###p.ylim([-55,-25])
###p.xlim([-12,-11])
##
###plt.legend()
#plt.show()
#
#####Field Perturbation####
#
#log_pDF0Results = log10(((pDF0Results)**2.0)**0.5)
#log_pDF20Results = log10(((pDF20Results)**2.0)**0.5)
#abs_log_fulloutput16 = log10(((fulloutput[:,16])**2.0)**0.5)
#abs_log_fulloutput17 = log10(((fulloutput[:,17])**2.0)**0.5)
#
#
#plt.plot(t_out,abs_log_fulloutput16,label=r'log $\delta \phi_1$')
#plt.plot(t_out,abs_log_fulloutput17,label=r'log $\delta \phi_2$')
#plt.title('Field perturbations')
#plt.xlabel('e-folds')
#plt.ylabel('log Field perturbations values')
##
#box = plt.subplot(111).get_position()
#plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
#plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
###p.ylim([-55,-25])
###p.xlim([-12,-11])
##
###plt.legend()
#plt.show()
#
##################################################
####All remaining values##########################
##################################################
#
####Phi###
#
#log_PhiResults = log10(((PhiResults)**2.0)**0.5)
#kkoPhiResults = ((PHIi)**(-1.0))*(((PhiResults)**2.0)**0.5)
#
##
#plt.plot(t_out,kkoPhiResults,label=r'((k/k_eq)^(3/2)) $\Phi$')
#plt.title('$\Phi$')
#plt.xlabel('e-folds')
#plt.ylabel('((k/k_eq)^(3/2)) $\Phi$')
##
#box = plt.subplot(111).get_position()
#plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
#plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
###p.ylim([-55,-25])
###p.xlim([-12,-11])
##
###plt.legend()
#plt.show()
#
#
####v1###
####v2###
#
#log_fulloutput14 = log10(((fulloutput[:,14])**2.0)**0.5)
#log_fulloutput15 = log10(((fulloutput[:,15])**2.0)**0.5)
#log_fulloutput13 = log10(((fulloutput[:,13])**2.0)**0.5)
#log_fulloutput12 = log10(((fulloutput[:,12])**2.0)**0.5)
#
##
#plt.plot(t_out,log_fulloutput14,label=r'$v_1 + B$')
#plt.plot(t_out,log_fulloutput[:,13],label=r'$v_b + B$')
#plt.plot(t_out,log_fulloutput[:,12],label=r'$v_r + B$')
#plt.plot(t_out,log_fulloutput15,label=r'$v_2 + B$')
#plt.title('3-velocities + B')
#plt.xlabel('e-folds')
#plt.ylabel('log velocity')
##
#box = plt.subplot(111).get_position()
#plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
#plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
###p.ylim([-55,-25])
###p.xlim([-12,-11])
##
###plt.legend()
#plt.show()
#
###############Log plots section! -end- #################
