#!/usr/bin/env python
# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# E X A M P L E   1   -   A   S I N G L E   R U N   W I T H               #
# O N E   S E T   O F   C O U P L I N G S   T O                           #
# D E M O N S T R A T I N G   P L O T T I N G   O F   P A R A M E T E R S #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy ODE solver 

#Imports section

import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import pylab as p #importing plotting functions
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from matplotlib import pyplot as plt
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin


from CONSTANTS import *
from MODELPAD import *
from BACKGROUND import *
from PERTURBED import *


####################Times############################
t_i=log(10.0**(-6.0));t_f=log(10.0**(0.5));step=0.01
#####################################################

##########ICs necessary for H and Potentials ########


f_0[0]=Or_o*Dc_o*(exp(-4.0*t_i));f_0[1]=Ob_o*Dc_o*(exp(-3.0*t_i))


ai=exp(t_i)
Hi= H(f_0,t_i)
VIi=0#VI(f_0,t_i)
V1i= 0#VIi[0]
V2i= 0#VIi[1]
VPi= 0#VP(f_0,t_i)
V1Pi = 0#VPi[0]
V2Pi = 0#VPi[1]
PHIi=(k/keq)**(-3.0/2.0)
Bi=-PHIi/(Hi*ai)
dDr0i=dDr0(f_0,t_i)
dDb0i=dDb0(f_0,t_i)


#######Remaining ICs dependent upon H and Potentials############

f_0[2]=-(2.0)*PHIi*f_0[0] + 4.0*Hi*ai*f_0[0]*Bi;f_0[3]=-(3.0/2.0)*PHIi*f_0[1] + 3.0*Hi*ai*f_0[1]*Bi
f_0[4]=((4.0*ai*Hi*f_0[0]+3.0*ai*Hi*f_0[1])**(-1.0))*(f_0[3] - 3.0*Hi*ai*f_0[1]*Bi + f_0[2] - 4.0*Hi*ai*f_0[0]*Bi + (2.0*(k**2.0)*PHIi)/((K*ai)**2.0)) + Bi
f_0[5]=f_0[4]
#########################
#####THE INTEGRATOR!#####
#########################

#Set up the time output required by ODE

t_out = np.arange(t_i,t_f,step) # start, end, interval


phtest=X(f_0,t_i)

Btest=Y(f_0,t_i)


#Now, actually do the numerical integration

###########################
####NEW ODE SOLVER#########
###########################

#set up the integrator
r = scipy.integrate.ode(df,jac=None).set_integrator('dopri5',rtol=1E-15,atol=1E-15,nsteps=1E8)#works, starts slower speeds steadily
#r = scipy.integrate.ode(df,jac=None).set_integrator('vode',method='bdf',rtol=1E-8,atol=1E-8,nsteps=1E8)#starts -very- slow, speeds up to high speed toward end
#r = scipy.integrate.ode(df,jac=None).set_integrator('lsoda', rtol=1E-12,atol=1E-12,nsteps=1E8)
#r = scipy.integrate.ode(df,jac=None).set_integrator('zvode', method='bdf')
#r = scipy.integrate.ode(df,jac=None).set_integrator('vode',method='bdf')
#r = scipy.integrate.ode(df,jac=None).set_integrator('dop853',rtol=1E-12,atol=1E-12,nsteps=1E8)#works, starts slower speeds steadily


r.set_initial_value(f_0,t_i) #feed it the initial values

#run the integrator - results to list
while r.successful() and r.t < t_f :
      print r.t
      if r.t==t_i : 
         fulloutput=np.array(r.y)

         inres=df(t_i,f_0)

         ZETAResults = ZETA(fulloutput,r.t)         
         PhiResults = X(fulloutput,r.t)

         BResults = Y(fulloutput,r.t)

         HResults = H(fulloutput,r.t)
         aResults = exp(r.t)
      else:
         res=np.array(r.y)

         fulloutput = np.vstack([fulloutput,res])
         ZETAi= ZETA(res,r.t)
         ZETAResults = np.vstack([ZETAResults,ZETAi])
         Xint= X(res,r.t)

         PhiResults = np.vstack([PhiResults,Xint])
         Bint= Y(res,r.t)

         BResults = np.vstack([BResults,Bint])
         Hint= H(res,r.t)
         HResults = np.vstack([HResults,Hint])
         aint= exp(r.t)
         aResults = np.vstack([aResults,aint])
      r.integrate(r.t+step)

##########################
####NEW ODE SOLVER END####
##########################

#######################################
#############save data#################
#######################################

out_name = "../Data/EXAMPLE1dp5OUTPUTPAD15"

np.save(out_name,fulloutput)

out_name2 = "../Data/EXAMPLE1dp5TIMEPAD15"

np.save(out_name2,t_out)

#######################################
#######################################
#######################################

##############Log plots section! -start- #################

####Background####

log_fulloutput = log10(((fulloutput)**2.0)**0.5)

log_bandrResults = log10(fulloutput[:,1]+fulloutput[:,0])


plt.plot(t_out,log_bandrResults,label=r'$(\rho_b + \rho_r )$')
plt.title('Background Densities')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
#p.ylim([-55,-25])
#p.xlim([-12,-11])

plt.legend()
plt.show()

####Perturbations####



abs_log_fulloutput9 = log10(((fulloutput[:,3])**2.0)**0.5)
abs_log_fulloutput8 = log10(((fulloutput[:,2])**2.0)**0.5)


plt.plot(t_out,abs_log_fulloutput9,label=r'$\delta \rho_b$')
plt.plot(t_out,abs_log_fulloutput8,label=r'$\delta \rho_r$')

plt.title('Perturbations')
plt.xlabel('e-folds')
plt.ylabel('log Energy Density')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####Flat Density Contrast####
dcon9 = (fulloutput[:,3]/fulloutput[:,1])
dcon8 = (fulloutput[:,2]/fulloutput[:,0])
kkodcon9=-((PHIi)**(-1.0))*dcon9
kkodcon8=-((PHIi)**(-1.0))*dcon8
plt.plot(t_out,kkodcon9,label=r'$\delta_b$')
plt.plot(t_out,kkodcon8,label=r'$\delta_r$')
plt.title('-$\Phi_i^{-1}$ Dens cont mad and rad Flat')
plt.xlabel('e-folds')
plt.ylabel(' dens cont')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####Long Density Contrast####

tshape = t_out.shape


for x in range(0,tshape[0]):
    
    if x==0 : 
        dcon9 = ((fulloutput[x,3] + 3.0*HResults[x]*aResults[x]*fulloutput[x,1]*BResults[x]/k**2.0)/fulloutput[x,1])
        dcon8 = ((fulloutput[x,2] + 4.0*HResults[x]*aResults[x]*fulloutput[x,0]*BResults[x]/k**2.0)/fulloutput[x,0])
    else:
        d9i = ((fulloutput[x,3] + 3.0*HResults[x]*aResults[x]*fulloutput[x,1]*BResults[x]/k**2.0)/fulloutput[x,1])
        dcon9 = np.vstack([dcon9,d9i])
        d8i = ((fulloutput[x,2] + 4.0*HResults[x]*aResults[x]*fulloutput[x,0]*BResults[x]/k**2.0)/fulloutput[x,0])
        dcon8 = np.vstack([dcon8,d8i])

kkodcon9=-((PHIi)**(-1.0))*dcon9
kkodcon8=-((PHIi)**(-1.0))*dcon8
plt.plot(t_out,kkodcon9,label=r'$\delta_b$')
plt.plot(t_out,kkodcon8,label=r'$\delta_r$')
plt.title('-$\Phi_i^{-1}$ Dens cont mad and rad long')
plt.xlabel('e-folds')
plt.ylabel(' dens cont')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()

####long sigma####

sigma=(3.0/4.0)*dcon8 - dcon9
plt.plot(t_out,log10(1.0+sigma),label=r'$\sigma$')
plt.title('long $\sigma$')
plt.xlabel('e-folds')
plt.ylabel('log_10(1+$\sigma$)')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()


####flat sigma####
dcon9 = (fulloutput[:,3]/fulloutput[:,1])
dcon8 = (fulloutput[:,2]/fulloutput[:,0])
sigma=(3.0/4.0)*dcon8 - dcon9
plt.plot(t_out,log10(1.0+sigma),label=r'$\sigma$')
plt.title('flat sigma')
plt.xlabel('e-folds')
plt.ylabel('log_10(1+sigma)')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()


####ZETA####

log_ZETAResults = log10(((ZETAResults)**2.0)**0.5)

plt.plot(t_out,log_ZETAResults,label=r'$\zeta$')

plt.title('Zeta')
plt.xlabel('e-folds')
plt.ylabel('log Zeta')

box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()


#################################################
###All remaining values##########################
#################################################

###Flat Phi###

kkoPhiResults = (((PhiResults)**2.0)**0.5)

#
plt.plot(t_out,kkoPhiResults,label=r'Flat $\Phi$')
plt.title('Flat $\Phi$')
plt.xlabel('e-folds')
plt.ylabel('Flat $\Phi$')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

###B###

kkoBResults = (((BResults)**2.0)**0.5)

#
plt.plot(t_out,kkoBResults,label=r'$B$')
plt.title('$B$')
plt.xlabel('e-folds')
plt.ylabel('$B$')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

###long Phi###

kkoLongPhiResults = ((PHIi)**(-1.0))*(((-HResults*BResults*aResults/(k**2.0))**2.0)**0.5)

#
plt.plot(t_out,kkoLongPhiResults,label=r'$\Phi_i^{-1} \Phi$ Long')
plt.title('Long$\Phi$')
plt.xlabel('e-folds')
plt.ylabel('$\Phi_i^{-1} \Phi$ Long')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc='center left', bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
##plt.legend()
plt.show()

out_name = "../Data/LONGPHIPAD15"

np.save(out_name,kkoLongPhiResults)

####vs####

abs_log_fulloutput5 = log10(((fulloutput[:,5])**2.0)**0.5)
abs_log_fulloutput4 = log10(((fulloutput[:,4])**2.0)**0.5)


plt.plot(t_out,abs_log_fulloutput5,label=r'$v_b$')
plt.plot(t_out,abs_log_fulloutput4,label=r'$v_r$')

plt.title('vs')
plt.xlabel('e-folds')
plt.ylabel('log v')
#
box = plt.subplot(111).get_position()
plt.subplot(111).set_position([box.x0, box.y0, box.width * 0.8, box.height])
plt.subplot(111).legend(loc=2, bbox_to_anchor=(1, 0.5))
##p.ylim([-55,-25])
##p.xlim([-12,-11])
#
plt.legend()
plt.show()


##############Log plots section! -end- #################
