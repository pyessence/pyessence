# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # #
# M O D E L . P Y #
# # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy / Dark Matter ODE solver

#This code contains model specific quantities. These includes constants which
#are model specific, the form of the potential being investigated, and the
#couplings.
import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin
from CONSTANTS import *

#########Set our wavenumber!#########
keq=5.76*(10.0**(-41.0))
kvko=10.0
k=kvko*H_o# First number is k relative to k_o, second expression is k_o
#####################################

########Sum of Exponentials Potential Specific ICs########

L1 = 0.0 # Lambda - exponent coefficient - for field 1

L=np.array([L1])

F = 2.87*(10**5.0)

Or_o = 2.47*(10.0**(-5.0))/(h**2.0) #Density param for rad
Ob_o = 0.022/(h**2.0) #Density param for baryons
wx1_o = -1.0 #eq of state for field 1
wx=np.array([wx1_o])
Ox1_o = 0.692 #Density param for field 1
Ox=np.array([Ox1_o])
ODM10_o = 1.0 - Ox1_o - Or_o - Ob_o #Density param for DM 1
x1_o = 0.0 # - field1 at current epoch

#########Set your couplings here###############################################################

C=np.array([[-0.0*(10.0**(-0.0))],[-0.0*(10.0**(-0.0))]])

###############################################################################################

dim = C.shape
print dim
I = 1
A = 1

In=np.zeros(13+3*(A-1)+4*(I-1))

f_0=np.zeros(13+3*(A-1)+4*(I-1))

#####DM EOS if required############################

w=np.zeros(A)

###################################################
print I
print A
########################################################################
#######################POTENTIAL # SET # UP#############################
########################################################################

##########Coupled Quintessence Sum of Exponentials Potenital############

def VI(In,t) : #potential for phi1 - Sum of exponentials
    return np.array([[-0.0*(10.0**(-0.0))],[-0.0*(10.0**(-0.0))]])   
    
def V(In,t) :
    VIin=VI(In,t)
    return Dc_o*Ox1_o

def VP(In,t) : #dwrt phi of pot 1 - Note this is sum of exponentials specific
    VIin=VI(In,t)
    return np.array([-0.0*(10.0**(-0.0))])

def VPP(In,t) : #2nd dwrt phi of pot1
    VIin=VI(In,t)
    return np.array([[-0.0*(10.0**(-0.0))],[-0.0*(10.0**(-0.0))]])
    
#Note this is sum of exponentials specific
