# -*- coding: utf-8 -*-

# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

# # # # # # # # # #
# M O D E L . P Y #
# # # # # # # # # #

"""
Created on Thu Sep 11 11:08:53 2014

@author: Alexander Leithes
"""

#Alexander Leithes Interacting Dark Energy / Dark Matter ODE solver

#This code contains model specific quantities. These includes constants which
#are model specific, the form of the potential being investigated, and the
#couplings.
import math #importing maths functions
import numpy as np #importing more maths functions. From now on this will be called np
import scipy #importing science functions 
import scipy.integrate #importing integrating function
import scipy.optimize #importing optimizing function
from numpy.polynomial import polynomial
from numpy import exp as exp
from numpy import log as log
from numpy import log10 as log10
from math import pi as pi
from math import cos as cos
from math import sin as sin
from CONSTANTS import *

#########Set our wavenumber!#########
keq=5.76*(10.0**(-41.0))
kvko=10.0
k=kvko*H_o# First number is k relative to k_o, second expression is k_o
#####################################


########Sum of Exponentials Potential Specific ICs########

L1 = 0.1*(10.0**(0.0)) # Lambda - exponent coefficient - for field 1
L2 = 0.1*(10.0**(0.0)) # Lambda - exponent coefficient - for field 2

L=np.array([L1,L2])

F = 2.87*(10**5.0)
T= (0.660/5.500)

Or_o = 2.47*(10.0**(-5.0))/(h**2.0) #Density param for rad
Ob_o = 0.022/(h**2.0) #Density param for baryons
wx1_o = -0.95 #eq of state for field 1
wx2_o = -0.99 #eq of state for field 2
wx=np.array([wx1_o,wx2_o])
Ox1_o = 0.032 #Density param for field 1
Ox2_o = 0.660 #Density param for field 2
Ox=np.array([Ox1_o,Ox2_o])
ODM10_o = 0.03 #Density param for DM 1
ODM20_o = (1.0 - ODM10_o - Ox1_o - Ox2_o - Or_o - Ob_o) #Density param for DM2
x1_o = ((1.0)/(K*L1))*(log((2.0)/(((K)**4.0)*Dc_o*Ox1_o*(1.0-wx1_o))))#valid for matter dominated only - field1 at current epoch
x2_o = ((1.0)/(K*L2))*(log((2.0)/(((K)**4.0)*Dc_o*Ox2_o*(1.0-wx2_o))))#valid for matter dominated only - field 2 at current epoch
x_o=np.array([x1_o,x2_o])
#print ODM10_o
#print ODM20_o
#print (ODM10_o + ODM20_o)*(h**2)

#########Set your couplings here###############################################################

C=np.array([[-0.2*(10.0**(-0.0)),0.4*(10.0**(-0.0))],[-0.3*(10.0**(-0.0)),0.6*(10.0**(-0.0))]])

###############################################################################################

dim = C.shape

I = dim[0]
A = dim[1]

In=np.zeros(13+3*(A-1)+4*(I-1))

f_0=np.zeros(13+3*(A-1)+4*(I-1))

#####DM EOS if required############################

w=np.zeros(A)

###################################################
print x1_o
print x2_o

print (1/(((K)**4.0)))*(exp(-L[0]*K*x1_o))
print (1/(((K)**4.0)))*(exp(-L[1]*K*x2_o))
print (1/(((K)**4.0)))*(exp(-L[0]*K*x1_o))/Dc_o
print (1/(((K)**4.0)))*(exp(-L[1]*K*x2_o))/Dc_o
print ((1/(((K)**4.0)))*(exp(-L[0]*K*x1_o)) + (1/(((K)**4.0)))*(exp(-L[1]*K*x2_o)))/Dc_o
########################################################################
#######################POTENTIAL # SET # UP#############################
########################################################################

##########Coupled Quintessence Sum of Exponentials Potenital############

def VI(In,t) : #potential for phi1 - Sum of exponentials
    return (1/((K**4.0)))*(exp(-L[0:I]*K*In[3+(A-1):3+(A-1)+(I-1)+1]))   
    
def V(In,t) :
    VIin=VI(In,t)
    return np.sum([VIin[0:I]])

def VP(In,t) : #dwrt phi of pot 1 - Note this is sum of exponentials specific
    VIin=VI(In,t)
    return -L[0:I]*K*VIin[0:I]

def VPP(In,t) : #2nd dwrt phi of pot1
    VIin=VI(In,t)
    return np.array([[((L[0]*K)**2.0)*VIin[0],0],[0,((L[1]*K)**2.0)*VIin[1]]])
    
#Note this is sum of exponentials specific
