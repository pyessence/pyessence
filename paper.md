---
title: 'Pyessence - Generalised Coupled Quintessence Linear Perturbation Python Code'
tags:
  - Python
  - Cosmology
  - Cold Dark Matter
  - Dark Energy
  - Cosmological Perturbation Theory
  - Coupled Quintessence
authors:
 - name: Alexander Leithes
   orcid: 0000-0001-9593-6757
   affiliation: Queen Mary, University of London.
date: 21 September 2016
bibliography: paper.bib
---

# Summary

Pyessence[\fn1][\fn2] is a Python code designed to allow the testing of linearly perturbed coupled quintessence models with multiple Cold Dark Matter fluid species and multiple Dark Energy scalar fields; assisted coupled quintessence (see e.g. [\fn3],[\fn4]). The equations coded are sufficiently general to allow for more exotic dark matter with a non-zero equation of state. Several example uses are included in the release in order to demonstrate typical functionality to the potential user. The user guide paper is [\fn2]. The first use science results paper is [\fn1]. Pyessence is released under an open source modified BSD license and is available on Bitbucket.

-![Pyessence deposited on Bitbucket.](bitbucket.org/pyessence/pyessence)

# References

[\fn1]: A.~Leithes, K.~A.~Malik, D.~J.~Mulryne and N.~J.~Nunes
  ``Linear Density Perturbations in Multifield Coupled Quintessence,''
  arXiv:1608.00908

[\fn2]: A.~Leithes,
  ``PYESSENCE - Generalised Coupled Quintessence Linear Perturbation Python Code - User Guide,''
  arXiv:1608.00910

[\fn3]: L.~Amendola, T.~Barreiro and N.~J.~Nunes,
  ``Assisted coupled quintessence,''
  arXiv:1407.2156.

[\fn4]: L.~Amendola,
  ``Perturbations in a coupled scalar field cosmology,''
  Mon.\ Not.\ Roy.\ Astron.\ Soc.\  {\bf 312}, 521 (2000).



