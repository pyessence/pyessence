# # # # # # # # # # # 
# P Y E S S E N C E #
# # # # # # # # # # # 

#Alexander Leithes Coupled Quintessence Linear Perturbations ODE solver

PYESSENCE is a package designed to evolve background and perturbed quantities within
coupled quintessence models.
The code is designed to be general, allowing the user to set-up a specific model with its
associated potential, numbers of field and fluid species, and investigate the evolution of
these systems.
The package is written for Python 2.7 and requires math, pylab, numpy and scipy packages be imported.
The design is modular and consists of:

CONSTANTS.py

#This code contains general constants to be called by the other Python 
#modules.

MODEL.py

#This code contains model specific quantities. These includes constants which
#are model specific, the form of the potential being investigated, and the
#couplings. It also contains the wavenumber to be investigated.

BACKGROUND.py

#The first section of this code contains the background equations which are not passed to the
#ODE integrator.
#The sceond section of this code contains the evolution equations for background quantities
#which are passed to the ODE integrator.


PERTURBED.py

#The first section of this code contains perturbed equations, in most cases constraints, which
#are not passed to the ODE integrator.
#The sceond section of this code contains the perturbed evolution equations which are passed
#to the ODE integrator. It also contains the overall function array
#passed to the ODE integrator.

Additionally, some EXAMPLEx.py codes are included demonstrating various uses of the
PYESSENCE package.


#########################################################################
################## A R R A Y   C O M P O N E N T S ######################
#########################################################################
#
#                    For and I fields A fluids
#
# In[0] = Dr0 - Background radiation density
# In[1] = Db0 - Backgroud baryon density
# In[2...2+(A-1)] = DM10...DM[A]0 - Background CDM fluid densities
# In[3+(A-1)...3+(A-1)+(I-1)] = x1...x[I] - Background field values
# In[4+(A-1)+(I-1)...4+(A-1)+2*(I-1)] = y1...y[I] - Background field time derivatives
# In[5+(A-1)+2*(I-1)] = pDr0 - Radiation density perturbation
# In[6+(A-1)+2*(I-1)] = pDb0 - Baryon density perturbation
# In[7+(A-1)+2*(I-1)...7+2*(A-1)+2*(I-1)] = pDM10...pDM[A] - CDM density perturbations
# In[8+2*(A-1)+2*(I-1)] = vr - Radiation 3-velocity + B
# In[9+2*(A-1)+2*(I-1)] = vb - Baryon 3-velocity + B
# In[10+2*(A-1)+2*(I-1)...10+3*(A-1)+2*(I-1)] = v1...v[A] - CDM 3-velocities + B
# In[11+3*(A-1)+2*(I-1)...11+3*(A-1)+3*(I-1)] = px1...px[I] - Field perturbations
# In[12+3*(A-1)+3*(I-1)...12+3*(A-1)+4*(I-1)] = py1...py[I] - Field perturbations time derivatives
#
# I is the number of fields in the model, the rows of the C matrix.
# A is the number of DM fluids in the model, the columns of the C matrix.
#########################################################################

A more detailed breakdown of the code is available on arXiv:xxxx.xxxx (PYESSENCE - Generalised Coupled Quintessence Linear Perturbation Python Code - A Guide, A. Leithes)

Note: Remember to include all the paths to the folders containing each python 
module when running your PYESSENCE codes.
